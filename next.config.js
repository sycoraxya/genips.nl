const path = require("path");
const withMDX = require("@next/mdx")({
    extension: /\.mdx?$/,
});

module.exports = withMDX({
    pageExtensions: ["tsx", "mdx", "ts"],
    sassOptions: {
        includePaths: [path.join(__dirname, "src", "styles")],
    },
    env: {
        env: process.env.APP_ENV || process.env.NODE_ENV,
    },
});
