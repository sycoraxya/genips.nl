import { StaticImageData } from "next/image";
import { MutableRefObject } from "react";
type ImgElementStyle = NonNullable<JSX.IntrinsicElements["img"]["style"]>;

export interface ImageProp {
    src:
        | string
        | StaticImageData
        | {
              default: StaticImageData;
          };
    alt: string;
    ref?: MutableRefObject<HTMLImageElement>;
    objectFit?: ImgElementStyle["objectFit"];
    objectPosition?: ImgElementStyle["objectPosition"];
}
