export interface TextTagProps {
    del?: boolean;
    mark?: boolean;
    italic?: boolean;
    underline?: boolean;
    code?: boolean;
}
