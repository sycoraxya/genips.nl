import { NextPage } from "next";
import { Layout } from "../components/layout";
import { Hero as HeroNew } from "@components/hero";
import { useAppContext } from "../hooks/useAppContext";
import { ContactBanner } from "@components/contactBanner";
import React from "react";
import { HeadMeta } from "../components/HeadMeta";
import { Col } from "@components/col";
import { Row } from "@components/row";

import styles from "@styles/aboutUs/aboutUs.module.scss";
import { Paragraph, Title } from "@components/typography";
import { AboutMe } from "@components/people/AboutMe";
import { TitleTextRow } from "@components/titleTextRow";

const AboutUs: NextPage = () => {
    useAppContext();

    const description =
        "Genips is een ontwerpbureau voor digitale producten. Wij ontwerpen eenvoudige en nuttige digitale oplossingen voor groeiende bedrijven.";

    return (
        <Layout>
            <HeadMeta
                title="Jouw digital product agency"
                description={description}
                image={"default_og_image.jpg"}
                canonicalSlug={"/over-ons"}
            />
            <HeroNew
                title={"Wij zijn Genips"}
                subtitle={"Jouw Digital Product Agency."}
                image={{ src: "about.svg", alt: "About Genips" }}
                body={
                    "Genips is een ontwerp- en ontwikkelingsbureau voor groeiende bedrijven. Wij creëren eenvoudige, nuttige en mooie digitale oplossingen. "
                }
            />
            <TitleTextRow title={"Wie we zijn"}>
                <Paragraph>
                    Wij zijn Genips: een ontwerp- en ontwikkelingsbureau voor digitale producten.
                    Genips bestaat uit een team van vrienden die klaar staan om de meest
                    ingewikkelde digitale puzzels op te lossen. We houden van eenvoudige en
                    efficiënte producten maken. Van elk project maken wij een succes.
                </Paragraph>
                <Paragraph>
                    Mainstream? Nee, dat is Genips zeker niet. Slimme, digitale oplossingen bedenken
                    is niet alleen ons werk. Het is onze passie. Wij maken digitale producten voor
                    mensen. Producten waar iedereen mee overweg kan. Elk project dat we aannemen
                    krijgt onze volle aandacht.
                </Paragraph>
            </TitleTextRow>
            {/* <AboutMe
                jobTitle={"Co-Founder & Digital Product Designer"}
                name={"Micha"}
                image={{ src: "micha-about-picture.jpg", alt: "Micha van Dijk" }}
                email={{ value: "mailto:micha@genips.nl" }}
                phone={{ value: "tel:+31625463010" }}
            >
                <Paragraph>
                    Tijdens mijn opleiding Communication & Multimedia Design heb ik van alle kanten
                    van het vak geproefd. Er zijn twee onderdelen die ik écht geweldig vindt: User
                    Experience Design en Commercieel Ontwerpen.
                </Paragraph>
                <Paragraph>
                    Naast mijn studie heb ik veel freelance opdrachten gedaan. Toch miste ik altijd
                    een partner om mee te sparren. Zo zijn Stefan en ik aan de praat geraakt over
                    onze eigen onderneming. En kijk eens waar we nu staan!
                </Paragraph>
            </AboutMe>
            <AboutMe
                jobTitle={"Co-Founder & Full Stack Developer"}
                name={"Stefan"}
                image={{
                    src: "stefan-about-picture.jpg",
                    alt: "Stefan Verweij",
                    objectPosition: "top",
                }}
                imageOrder={1}
                email={{ value: "mailto:stefan@genips.nl" }}
                phone={{ value: "tel:+31629769545" }}
            >
                <Paragraph>
                    Na vijf jaar in loondienst was het tijd voor iets anders. Micha en ik raakten
                    daarom aan de praat over het starten van ons eigen bedrijf. Inmiddels is Genips
                    gegroeid tot een creatief bureau waar ik de kans krijg om gave concepten en
                    technieken toe te passen.
                </Paragraph>
                <Paragraph>
                    Ik ben altijd hard bezig om nieuwe ideeën te ontwikkelen. Zo komen we telkens
                    weer tot een solide resultaat en een praktische oplossing voor de klant. React,
                    Javascript, Typescript en Node kennen geen geheimen voor mij.
                </Paragraph>
            </AboutMe> */}
            <ContactBanner />
        </Layout>
    );
};

export default AboutUs;
