import { NextPage } from "next";
import React from "react";
import {
    TableRows,
    Package,
    PackageType,
    PackagesTable,
} from "../../components/packagesTable/packagesTable";
import { Hero } from "@components/hero";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";
import { ContactBanner } from "@components/contactBanner";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";

const Seo: NextPage = () => {
    useAppContext({ heroColor: "light" });

    const description =
        "Tijd om te beginnen met je online vindbaarheid. Schaf nu jouw eigen domeinnaam aan!";

    const { rows, packages } = DomeinnaamData();

    return (
        <Layout overlay={false}>
            <HeadMeta
                title="Begin met je online vindbaarheid door een domeinnaam"
                description={description}
                canonicalSlug={"/services/domeinnaam"}
                image={"default_og_image.jpg"}
            />
            <Hero
                title="Domeinnaam"
                subtitle="Het begin is het moeilijkste."
                image={{ src: "/domeinnaam.svg", alt: "domeinnaam" }}
                body="Gevonden worden op het internet is het begin van elk modern bedrijf. Dit kan natuurlijk niet zonder een domeinnaam."
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />
            <PackagesTable packages={packages} rows={rows} />
            <ContactBanner heading="Ook een domeinnaam nodig?" gaPageName="Domeinnaam" />
        </Layout>
    );
};

export default Seo;

const DomeinnaamData = () => {
    enum Row {
        nl = ".nl",
        com = ".com",
        eu = ".eu",
        be = ".be",
        net = ".net",
        de = ".de",
        org = ".org",
        info = ".info",
        nu = ".nu",
        fr = ".fr",
        app = ".app",
        dev = ".dev",
        online = ".online",
        shop = ".shop",
        studio = ".studio",
        io = ".io",
        rocks = ".rocks",
        biz = ".biz",
        es = ".es",
        health = ".health",
    }

    const rows: TableRows = [
        Row.nl,
        Row.com,
        Row.eu,
        Row.be,
        Row.net,
        Row.de,
        Row.org,
        Row.info,
        Row.nu,
        Row.fr,
        Row.app,
        Row.dev,
        Row.online,
        Row.shop,
        Row.studio,
        Row.io,
        Row.rocks,
        Row.biz,
        Row.es,
        Row.health,
    ];

    const packages: Package[] = [
        {
            type: PackageType.Jaarlijks,
            rows: [
                { key: Row.nl, value: "€ 17,-" },
                { key: Row.com, value: "€ 27,-" },
                { key: Row.eu, value: "€ 27,-" },
                { key: Row.be, value: "€ 22,-" },
                { key: Row.net, value: "€ 27,-" },
                { key: Row.de, value: "€ 25,-" },
                { key: Row.org, value: "€ 28,-" },
                { key: Row.info, value: "€ 26,-" },
                { key: Row.nu, value: "€ 40,-" },
                { key: Row.fr, value: "€ 28,-" },
                { key: Row.app, value: "€ 39,-" },
                { key: Row.dev, value: "€ 30,-" },
                { key: Row.online, value: "€ 67,-" },
                { key: Row.shop, value: "€ 64,-" },
                { key: Row.studio, value: "€ 47,-" },
                { key: Row.io, value: "€ 76,-" },
                { key: Row.rocks, value: "€ 26,-" },
                { key: Row.biz, value: "€ 35,-" },
                { key: Row.es, value: "€ 28,-" },
                { key: Row.health, value: "€ 139,-" },
            ],
        },
    ];

    return { rows, packages };
};
