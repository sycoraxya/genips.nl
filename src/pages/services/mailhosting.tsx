import { GetServerSideProps, NextPage } from "next";
import React from "react";

const Mailhosting: NextPage = () => {
    return <></>;
};

export default Mailhosting;

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
    res.writeHead(301, {
        Location: "/services/hosting",
    });

    res.end();
    return { props: {} };
};
