import { NextPage } from "next";
import Masonry from "react-masonry-css";
import { Service } from "../../components/services/service";
import React, { useRef, useEffect } from "react";
import anime from "animejs";
import serviceStyles from "../../styles/5-components/services/service.module.scss";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";
import { Hero } from "@components/hero";
import styles from "@styles/services/index.module.scss";
import { Col, Row } from "@components/grid";
import gridHelperStyles from "@styles/grid/helpers.module.scss";

const Services: NextPage = () => {
    useAppContext();

    const description =
        "We ontwerpen en ontwikkelen niet alleen geoptimaliseerde websites. We helpen met hosting, onderhoud en verbetering van alle digitale producten.";

    const services = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const timeline = anime.timeline({
            delay: 400,
            easing: "easeInOutQuad",
        });
        const servicesCards = services.current.querySelectorAll(`.${serviceStyles.root}`);

        timeline.add(
            {
                targets: [servicesCards],
                opacity: [0, 1],
                translateY: [50, 0],
                duration: 300,
                delay: anime.stagger(50),
                complete: () => {
                    servicesCards.forEach((serviceCard: HTMLElement) => {
                        serviceCard.style.transition = "transform 0.4s ease";
                    });
                },
            },
            "+=200",
        );
    }, []);

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Websites, webshops en meer digitale producten "
                description={description}
                canonicalSlug={"/services"}
                image="/default_og_image.jpg"
            />

            <Hero />
            <div className={styles.bgWrapper}>
                <TitleTextRow title={"Onze services"} textColor="light">
                    <Paragraph>
                        We ontwerpen en ontwikkelen niet alleen geoptimaliseerde websites. We helpen
                        met hosting, onderhoud en verbetering van alle digitale producten.
                    </Paragraph>
                </TitleTextRow>
            </div>

            <Row containerClassName={`${styles.bgWrapper}`}>
                <Col span={12}>
                    <div ref={services}>
                        <Masonry
                            breakpointCols={{
                                default: 4,
                                1440: 3,
                                1200: 2,
                                768: 1,
                            }}
                            className={`${styles.masonryContainer} ${gridHelperStyles.bottomSpacing}`}
                            columnClassName={styles.masonryContainerColumn}
                        >
                            <Service title="Websites" url="/services/websites">
                                We bouwen websites met een passie. Jij bent betrokken bij het hele
                                proces, van ontwerp tot uitvoering.
                            </Service>
                            <Service title="Webshops" megaTitle="Shops" url="/services/webshops">
                                Genips ontwerpt een webshop die niet alleen je conversie een boost
                                geeft maar je bedrijf ook naar een hoger niveau tilt.
                            </Service>
                            <Service
                                title="Webapplicaties"
                                megaTitle="Webapplicaties"
                                url="/services/webapplicaties"
                            >
                                Sta je voor een complex digitaal vraagstuk en wil je dit oplossen
                                met een webapplicatie? Laat ons weten wat je zoekt en we bouwen het
                                voor je.
                            </Service>
                            <Service
                                title="Shared Hosting"
                                megaTitle="Shared"
                                url="/services/shared-hosting"
                            >
                                Een snelle en veilige website begint bij goede hosting. Genips zorgt
                                voor professionele mailadressen en een betrouwbaar hostingplatform.
                            </Service>
                            <Service
                                title="Domeinnaam"
                                megaTitle="Domeinnaam"
                                url="/services/domeinnaam"
                            >
                                Tijd om te starten met jouw online vindbaarheid. Daar heb je
                                natuurlijk een domeinnaam voor nodig. Laten we jou op weg helpen!
                            </Service>
                            <Service
                                title="Onderhoud & Optimalisatie"
                                megaTitle="Onderhoud"
                                url="/services/onderhoud"
                            >
                                Een website moet je onderhouden. Wij nemen het onderhoud graag van
                                je over en zorgen dat alles altijd in orde is. Hackers blijven
                                buiten de deur!
                            </Service>
                            <Service
                                title="Conversie optimalisatie"
                                megaTitle="Conversie"
                                url="/services/conversie-optimalisatie"
                            >
                                Meer kliks op je advertentie, meer telefoontjes ontvangen of meer
                                aanvragen in je mailbox: conversie optimaliseren zijn wij de beste
                                in. Laten we samen de uitdaging aangaan!
                            </Service>
                            <Service
                                title="Dedicated Hosting"
                                megaTitle="Dedicated"
                                url="/services/dedicated-hosting"
                            >
                                Dedicated hosting biedt krachtige en exclusieve serverresources voor
                                optimale prestaties en beveiliging, ideaal voor bedrijven die
                                maximale controle en betrouwbaarheid eisen.
                            </Service>
                        </Masonry>
                    </div>
                </Col>
            </Row>

            {/* <div className={oldStyles.root} ref={services}></div> */}
        </Layout>
    );
};

export default Services;
