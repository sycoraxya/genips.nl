import { NextPage } from "next";
import React from "react";
import { Hero } from "@components/hero";
import { ContactBanner } from "@components/contactBanner";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";
import { PortfolioSVM } from "src/components/portfolio/PortfolioSVM";
import { PortfolioEye } from "src/components/portfolio/PortfolioEye";
import { PortfolioPhonecam } from "src/components/portfolio/PortfolioPhonecam";
import { PortfolioBasicproducts } from "src/components/portfolio/PortfolioBasicproducts";

const Webapplicaties: NextPage = () => {
    useAppContext({ heroColor: "white" });

    const description =
        "Je staat voor een grote digitale uitdaging en wil een webapplicatie om het op te lossen? Wij gaan de uitdaging graag aan.";

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Los digitale vraagstukken op met webapplicaties"
                description={description}
                canonicalSlug={"/services/webapplicaties"}
                image={"default_og_image.jpg"}
            />

            <Hero
                title={"Webapplicaties"}
                subtitle={"Moeilijke dingen simpel maken."}
                image={{ src: "/webapplicaties.svg", alt: "webapplicaties" }}
                body={
                    "Je staat voor een digitale uitdaging: grote bestanden verwerken, een online evenement of een interne webshop. Cool! Laten we samen gaan ontdekken hoe we dit kunnen realiseren."
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />
            <PortfolioSVM imagePositionOverride="right" />
            <PortfolioEye imagePositionOverride="left" />
            <PortfolioPhonecam />
            <PortfolioBasicproducts />
            <ContactBanner
                heading="Tijd om complex makkelijk te maken?"
                gaPageName="Webapplicaties"
            />
        </Layout>
    );
};

export default Webapplicaties;
