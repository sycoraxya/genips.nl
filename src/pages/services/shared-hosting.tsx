import { NextPage } from "next";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import {
    TableRows,
    Package,
    PackageType,
    PackagesTable,
} from "../../components/packagesTable/packagesTable";
import React from "react";
import { HeadMeta } from "src/components/HeadMeta";
import { Hero } from "@components/hero";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";
import { ContactBanner } from "@components/contactBanner";
import { Button, ButtonType } from "@components/button";

const Hosting: NextPage = () => {
    useAppContext({ heroColor: "light" });

    const description =
        "Door goede hosting is je website sneller én veiliger. Laat Genips daarom nu jouw hosting overnemen.";

    enum Row {
        Cost = "Maandelijks",
        Memory = "PHP Geheugen",
        vCPUs = "CPUs",
        SsdStorage = "NVMe SSD Opslag",
        MonthlyBandwidth = "Maandelijks Dataverkeer",
        EmailAccounts = "E-mailadressen",
        MailboxSize = "Mailboxgrootte",
        Webmail = "Webmail",
        SSL = "SSL Certificaat",
        Cloudflare = "Cloudflare",
        HSTS = "HSTS",
        EndpointMonitoring = "Endpoint Monitoring",
        Website = "Website",
        Webshop = "Webshop",
    }

    const rows: TableRows = [
        {
            key: Row.Cost,
        },
        { type: "heading", value: "Hosting" },
        {
            key: Row.Memory,
        },
        {
            key: Row.vCPUs,
        },
        {
            key: Row.SsdStorage,
            description:
                "Het aantal opslagruimte dat je kunt gebruiken voor je website. Denk hierbij aan bestanden, afbeeldingen, video's, databases en e-mails.",
        },
        {
            key: Row.MonthlyBandwidth,
            description: "Het aantal dataverkeer dat je kunt gebruiken voor je website.",
        },
        { type: "heading", value: "E-mail" },
        {
            key: Row.EmailAccounts,
            description: "Het aantal e-mailadressen dat je kunt gebruiken voor je website.",
        },
        {
            key: Row.MailboxSize,
        },
        {
            key: Row.Webmail,
            description:
                "Met webmail kun je e-mails lezen en versturen zonder een e-mailprogramma te gebruiken.",
        },
        { type: "heading", value: "Beveiliging" },
        {
            key: Row.SSL,
        },
        {
            key: Row.Cloudflare,
        },
        {
            key: Row.HSTS,
        },
        {
            key: Row.EndpointMonitoring,
        },
        { type: "heading", value: "Geschikt voor" },
        {
            key: Row.Website,
        },
        {
            key: Row.Webshop,
        },
    ];

    const packages: Package[] = [
        {
            type: PackageType.Basic,
            rows: [
                { key: Row.Cost, value: "€ 11,-" },
                { key: Row.Memory, value: "256 MB" },
                { key: Row.vCPUs, value: "1 CPU" },
                { key: Row.SsdStorage, value: "10 GB" },
                { key: Row.MonthlyBandwidth, value: "50 GB" },
                { key: Row.EmailAccounts, value: "3" },
                { key: Row.MailboxSize, value: "1 GB" },
                { key: Row.Webmail, value: true },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.EndpointMonitoring, value: true },
                { key: Row.Website, value: true },
                { key: Row.Webshop, value: false },
            ],
        },
        {
            type: PackageType.Business,
            rows: [
                { key: Row.Cost, value: "€ 16,-" },
                { key: Row.Memory, value: "512 MB" },
                { key: Row.vCPUs, value: "2 CPU" },
                { key: Row.SsdStorage, value: "15 GB" },
                { key: Row.MonthlyBandwidth, value: "Onbeperkt" },
                { key: Row.EmailAccounts, value: "10" },
                { key: Row.MailboxSize, value: "3 GB" },
                { key: Row.Webmail, value: true },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.EndpointMonitoring, value: true },
                { key: Row.Website, value: true },
                { key: Row.Webshop, value: false },
            ],
        },
        {
            type: PackageType.Grow,
            rows: [
                { key: Row.Cost, value: "€ 26,-" },
                { key: Row.Memory, value: "768 MB" },
                { key: Row.vCPUs, value: "3 CPUs" },
                { key: Row.SsdStorage, value: "25 GB" },
                { key: Row.MonthlyBandwidth, value: "Onbeperkt" },
                { key: Row.EmailAccounts, value: "30" },
                { key: Row.MailboxSize, value: "5 GB" },
                { key: Row.Webmail, value: true },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.EndpointMonitoring, value: true },
                { key: Row.Website, value: true },
                { key: Row.Webshop, value: true },
            ],
        },
    ];

    return (
        <Layout overlay={false}>
            <HeadMeta
                title="Razendsnelle website- en mailhosting"
                description={description}
                canonicalSlug={"/services/shared-hosting"}
                image={"default_og_image.jpg"}
            />
            <Hero
                title={"Shared Hosting"}
                subtitle={"Razendsnelle website en mailservers."}
                image={{ src: "/shared-hosting.svg", alt: "shared hosting" }}
                body={
                    "Met goede hosting is je website sneller en veiliger. Wij zorgen er bovendien voor dat je veilig bent tegen hackers en beter vindbaar in Google."
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />
            <TitleTextRow title={"Waarom kiezen voor shared hosting bij Genips?"}>
                <Paragraph>
                    Genips biedt niet alleen betrouwbare hosting. We zorgen ook voor snelle support
                    wanneer er problemen zijn. We laten je niet aan je lot over, maar lossen het
                    probleem voor je op. Wil je zelf ook een oogje in zeil houden? We hebben een
                    handige website waar je de status van onze server kunt checken. Zo weet je
                    gelijk of we al op de hoogte zijn van een mogelijke storing.
                </Paragraph>
                <Paragraph>
                    Bij al onze hostingpakketten zitten mailaccounts inbegrepen, beginnende bij drie
                    accounts in het kleinste pakket. Heb je meer mailaccounts nodig? Neem dan
                    contact met ons op.
                </Paragraph>
                <Paragraph>
                    Iedere klant ontvangt bovendien een paar extra’s. Denk dan aan een SSL
                    certificaat zodat je website versleuteld en veilig is. Ook rekenen wij geen
                    verhuiskosten als je vanaf een andere provider komt. Daarnaast gebruiken wij de
                    laatste SQL en PHP versies. Dat zorgt voor een razendsnelle en betrouwbare
                    website.
                </Paragraph>
                <br />
                <Button
                    label="Op zoek naar een domeinnaam?"
                    type={ButtonType.Secondary}
                    href="/services/domeinnaam"
                />
            </TitleTextRow>
            <PackagesTable packages={packages} rows={rows} />
            <ContactBanner heading="Klaar om te starten?" gaPageName="shared-hosting" />
        </Layout>
    );
};

export default Hosting;
