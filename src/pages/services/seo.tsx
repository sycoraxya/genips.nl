import { GetServerSideProps, NextPage } from "next";
import React from "react";

const Seo: NextPage = () => {
    return <></>;
};

export default Seo;

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
    res.writeHead(301, {
        Location: "/services",
    });

    res.end();
    return { props: {} };
};
