import { NextPage } from "next";
import React from "react";
import { PortfolioStudioSchneemann } from "../../components/portfolio/PortfolioStudioSchneemann";
import { Layout } from "@components/Layout";
import { HeadMeta } from "src/components/HeadMeta";
import { Hero } from "@components/hero";
import { useAppContext } from "@hooks/useAppContext";
import { ContactBanner } from "@components/contactBanner";
import { PortfolioAddVue } from "src/components/portfolio/PortfolioAddVue";
import { PortfolioAntilopen } from "src/components/portfolio/PortfolioAntilopen";
import { PortfolioSannewevers } from "src/components/portfolio/PortfolioSannewevers";
import { PortfolioBijzonderecamping } from "src/components/portfolio/PortfolioBijzonderecamping";
import { PortfolioNewcold } from "src/components/portfolio/PortfolioNewcold";
import { PortfolioPromentum } from "src/components/portfolio/PortfolioPromentum";
import { PortfolioVerne } from "src/components/portfolio/PortfolioVerne";

const Websites: NextPage = () => {
    useAppContext({ heroColor: "white" });

    const description =
        "Op zoek naar een effectieve en gebruiksvriendelijke website? Genips creëert de website die écht bij jouw bedrijf past.";

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Voor een efficiënte en simpele website"
                description={description}
                canonicalSlug={"/services/websites"}
                image={"default_og_image.jpg"}
            />

            <Hero
                title={"Websites"}
                image={{ src: "/websites.svg", alt: "websites" }}
                subtitle={"Op naar de volgende stap voor je bedrijf."}
                body={
                    "Wij vinden dat digitale producten efficiënt en simpel moeten zijn. Met geavanceerde technologieën biedt Genips een groot aantal digitale diensten, waaronder slim webdesign, UX/UI-verbetering en bedrijfsoplossingen die grenzen verleggen. Bekijk ons portfolio en laat je overtuigen."
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
                button={{
                    label: "Onderhoud- & optimalisatie voor je website",
                    href: {
                        pathname: "/services/onderhoud",
                        query: { activeTable: "website" },
                    },
                }}
            />

            <PortfolioSannewevers />
            <PortfolioAntilopen imagePositionOverride="left" />
            <PortfolioStudioSchneemann imagePositionOverride="right" />
            <PortfolioBijzonderecamping imagePositionOverride="left" />
            <PortfolioNewcold imagePositionOverride="right" />
            <PortfolioPromentum />
            <PortfolioVerne />
            <PortfolioAddVue imagePositionOverride="left" />
            <ContactBanner heading="Eigen website beginnen?" gaPageName="websites" />
        </Layout>
    );
};

export default Websites;
