import { ContactBanner } from "@components/contactBanner";
import { Hero } from "@components/hero";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { NextPage } from "next";
import { HeadMeta } from "src/components/HeadMeta";

const Branding: NextPage = () => {
    useAppContext({ heroColor: "white" });

    const description =
        "Succesvolle online branding is de sleutel tot succes. Van zzp’ers tot grote bedrijven: wij ontwikkelen een merk dat bij je past.";

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Maak je bedrijf helemaal eigen"
                description={description}
                canonicalSlug={"/services/branding"}
                image={"default_og_image.jpg"}
            />

            <Hero
                title={"Branding"}
                subtitle={"Maak je bedrijf helemaal eigen."}
                body={
                    "Hoe groot of klein een bedrijf ook is, succesvolle branding is essentieel. Vooral als het gaat om digitale branding. Van startups en zzp’ers tot gevestigde ondernemingen: wij zoeken net zo lang totdat we een merk hebben ontwikkeld dat echt bij je past."
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />

            <ContactBanner heading="Tijd voor een (re)branding?" gaPageName="branding" />
        </Layout>
    );
};

export default Branding;
