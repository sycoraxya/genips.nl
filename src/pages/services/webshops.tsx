import { NextPage } from "next";
import React from "react";
import { Hero } from "@components/hero";
import { ContactBanner } from "@components/contactBanner";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";
import { PortfolioLuna } from "src/components/portfolio/PortfolioLuna";
import { PortfolioHaasF1 } from "src/components/portfolio/PortfolioHaasF1";
import { PortfolioRetourkoop } from "src/components/portfolio/PortfolioRetourkoop";
import { PortfolioGlobos } from "src/components/portfolio/PortfolioGlobos";

const Webshops: NextPage = () => {
    useAppContext({ heroColor: "white" });

    const description =
        "Op zoek naar een overzichtelijke webshop met hoge conversie? Genips bouwt de webshop van je dromen waarmee je meer klanten bereikt.";

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Voor overzichtelijke webshops met hoge conversie"
                description={description}
                canonicalSlug={"/services/webshops"}
                image={"default_og_image.jpg"}
            />

            <Hero
                title={"Webshops"}
                subtitle={"De perfecte online winkel."}
                image={{ src: "/webshops.svg", alt: "webshops" }}
                body={
                    "Het maken van een webshop is complex, zeker als je veel producten verkoopt. Laat ons helpen er een overzichtelijk geheel van te maken. Samen ontwerpen we de webshop van je dromen en optimaliseren we conversie."
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
                button={{
                    label: "Onderhoud- & optimalisatie voor je webshop",
                    href: {
                        pathname: "/services/onderhoud",
                        query: { activeTable: "webshop" },
                    },
                }}
            />
            <PortfolioRetourkoop />
            <PortfolioHaasF1 imagePositionOverride="left" />
            <PortfolioLuna />
            <PortfolioGlobos imagePositionOverride="left" />
            <ContactBanner heading="Klaar om een webshop te openen?" gaPageName="webshops" />
        </Layout>
    );
};

export default Webshops;
