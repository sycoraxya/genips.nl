import { GetServerSideProps, NextPage } from "next";
import {
    TableRows,
    Package,
    PackageType,
    PackagesTable,
} from "../../components/packagesTable/packagesTable";
import React from "react";
import { Hero } from "@components/hero";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";
import { ContactBanner } from "@components/contactBanner";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";

interface OnderhoudPageProps {}

const Onderhoud: NextPage<OnderhoudPageProps> = () => {
    useAppContext({ heroColor: "light" });

    const description =
        "Met een mooie website ben je er niet: je moet onderhouden en optimaliseren voor de beste vindbaarheid en conversie. Laat Genips helpen.";

    const { rows, packages } = websiteOnderhoudData();

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Besteed onderhoud en optimalisatie van je website uit"
                description={description}
                canonicalSlug={"/services/onderhoud"}
                image={"default_og_image.jpg"}
            />
            <Hero
                title="Onderhoud & optimalisatie"
                subtitle="Laat het lastige aan ons over."
                image={{
                    src: "/onderhoud-en-optimalisatie.svg",
                    alt: "onderhoud en optimalisatie",
                }}
                body="Een gebrekkige website? Dat wil niemand. Daarom zorgen wij ervoor dat je website in veilige handen is. We verkleinen de kans dat je gehackt wordt en zorgen dat alles blijft werken."
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />
            <TitleTextRow title={"Waarom onderhoud en optimalisatie nodig is"}>
                <Paragraph>
                    Als je nooit je fiets smeert, breekt op een gegeven moment je ketting. Een
                    website is precies hetzelfde: als je deze nooit een onderhoudsbeurt geeft, loop
                    je grote risico’s. Je website is bijvoorbeeld niet meer bereikbaar of je wordt
                    gehackt.
                </Paragraph>

                <Paragraph>
                    Wij zorgen ervoor dat je website op tijd de juiste updates ontvangt. Plugins,
                    thema’s en andere elementen van je website worden op tijd vernieuwd. Daarnaast
                    maken we backups voor als er toch iets fout gaat.
                </Paragraph>

                <Paragraph>
                    Specifiek voor webshops heeft Genips een aanvullend aanbod. Wil je meer verkopen
                    in jouw webshop? Laat onze CRO (Conversion Rate Optimisation) experts je verder
                    helpen door middel van A/ B-, multivariant- en personalisatietesten.
                </Paragraph>
            </TitleTextRow>
            <PackagesTable packages={packages} rows={rows} />
            <ContactBanner
                heading="Tijd voor een veilige en geoptimaliseerde website of webshop?"
                gaPageName="onderhoud"
            />
        </Layout>
    );
};

export const getServerSideProps: GetServerSideProps<OnderhoudPageProps> = async ({ query }) => {
    const activeTable =
        (Array.isArray(query.activeTable) ? query.activeTable[0] : query.activeTable) || "website";

    return {
        props: { activeTable },
    };
};

export default Onderhoud;

const websiteOnderhoudData = () => {
    enum Row {
        Cost = "Maandelijks",
        Backups = "Dagelijkse Backups",
        StorageCycle = "90 Dagen Opgeslagen",
        PasswordProtection = "Wachtwoordbeveiliging",
        UpdateFrequency = "Automatische Updates",
        VisualRegressionTesting = "Visuele Regressietesten",
        AutoBackup = "Pre-Update Backup",
        Caching = "Pagina Caching",
        LazyLoading = "Lazy Loading",
        DatabaseOptimalisation = "Database Optimalisatie",
        ImageOptimalisation = "Afbeelding Compressie",
        LoginProtection = "Inlogbeveiliging",
        MalwareScanner = "Malwarescanner",
        VulnerabilityScanner = "Kwetsbaarheidsscanner",
        ActivityLog = "Activiteitenlogboek",
        UptimeMonitoring = "Uptime Monitoring",
        Staging = "Staging Website",
        MFA = "Tweestapsverificatie",
        Cookiebot = "Cookiebot",
        CloudflarePro = "Cloudflare Pro",
    }

    const rows: TableRows = [
        {
            key: Row.Cost,
        },
        { type: "heading", value: "Backups" },
        {
            key: Row.Backups,
            description: "We maken dagelijkse backups van jouw website of webshop.",
        },
        {
            key: Row.StorageCycle,
            description: "Dit is het aantal dagen dat we de backups opslaan.",
        },
        {
            key: Row.PasswordProtection,
            description:
                "Onze backups hebben allemaal een uniek wachtwoord, zodat niemand anders ermee aan de haal kan gaan.",
        },
        { type: "heading", value: "Updates" },
        {
            key: Row.UpdateFrequency,
            description: "Dit is het aantal updates die we uitvoeren over een bepaalde tijd.",
        },

        {
            key: Row.AutoBackup,
            description: "Maak automatisch een backup voordat je een update uitvoert.",
        },
        {
            key: Row.VisualRegressionTesting,
            description: "Controleren van visuele aanpassingen in de website na updaten.",
        },
        { type: "heading", value: "Performance" },
        {
            key: Row.Caching,
            description:
                "Caching zorgt ervoor dat je website sneller laadt. Dit is belangrijk voor de gebruikerservaring en de vindbaarheid in zoekmachines.",
        },
        {
            key: Row.LazyLoading,
            description: "Laat afbeeldingen pas laden als ze in beeld zijn.",
        },
        {
            key: Row.DatabaseOptimalisation,
            description: "Dagelijkse optimalisatie van je database.",
        },
        {
            key: Row.ImageOptimalisation,
            description:
                "Converteer afbeeldingen naar WebP formaat en verklein de bestandsgrootte.",
        },
        { type: "heading", value: "Beveiliging" },
        {
            key: Row.LoginProtection,
            description: "Bescherm het inloggen en registeren tegen aanvallen van buitenaf.",
        },
        {
            key: Row.MalwareScanner,
            description: "Scan je website op malware en andere kwetsbaarheden.",
        },
        {
            key: Row.VulnerabilityScanner,
            description: "Realtime controle op bekende kwetsbaarheden.",
        },
        {
            key: Row.ActivityLog,
            description:
                "Bekijk een overzicht van alle wijzigingen die zijn uitgevoerd en door wie.",
        },
        {
            key: Row.UptimeMonitoring,
            description:
                "Realtime controle van downtime en automatische meldingen wanneer er iets aan de hand is.",
        },
        { type: "heading", value: "Extra's" },
        {
            key: Row.Staging,
            description: "Op aanvraag een testomgeving, zodat je nieuwe functies kunt proberen.",
        },
        {
            key: Row.MFA,
            description: "Extra beveiliging met 2FA / MFA codes.",
        },
        {
            key: Row.Cookiebot,
            description: "GDPR en AVG vriendelijke implementatie van cookies en Consent Mode v2.",
        },
        {
            key: Row.CloudflarePro,
            description: "Extra snelheid en beveiliging door Cloudflare.",
        },
    ];

    const packages: Package[] = [
        {
            type: PackageType.Basic,
            rows: [
                { key: Row.Cost, value: "€ 10,-" },
                { key: Row.Backups, value: true },
                { key: Row.StorageCycle, value: true },
                { key: Row.PasswordProtection, value: true },
                { key: Row.UpdateFrequency, value: "Maandelijks" },
                { key: Row.VisualRegressionTesting, value: false },
                { key: Row.AutoBackup, value: false },
                { key: Row.ActivityLog, value: true },
                { key: Row.Caching, value: false },
                { key: Row.LazyLoading, value: false },
                { key: Row.DatabaseOptimalisation, value: false },
                { key: Row.ImageOptimalisation, value: false },
                { key: Row.LoginProtection, value: false },
                { key: Row.MalwareScanner, value: false },
                { key: Row.VulnerabilityScanner, value: false },
                { key: Row.ActivityLog, value: false },
                { key: Row.UptimeMonitoring, value: true },
                { key: Row.Staging, value: false },
                { key: Row.MFA, value: false },
                { key: Row.Cookiebot, value: "vanaf € 15,- per maand" },
                { key: Row.CloudflarePro, value: "+ € 30,- per maand" },
            ],
        },
        {
            type: PackageType.Business,
            description: "Meest gekozen!",
            rows: [
                { key: Row.Cost, value: "€ 30,-" },
                { key: Row.Backups, value: true },
                { key: Row.StorageCycle, value: true },
                { key: Row.PasswordProtection, value: true },
                { key: Row.UpdateFrequency, value: "Wekelijks" },
                { key: Row.VisualRegressionTesting, value: false },
                { key: Row.AutoBackup, value: false },
                { key: Row.ActivityLog, value: true },
                { key: Row.Caching, value: true },
                { key: Row.LazyLoading, value: true },
                { key: Row.DatabaseOptimalisation, value: true },
                { key: Row.ImageOptimalisation, value: false },
                { key: Row.LoginProtection, value: false },
                { key: Row.MalwareScanner, value: false },
                { key: Row.VulnerabilityScanner, value: false },
                { key: Row.ActivityLog, value: false },
                { key: Row.UptimeMonitoring, value: true },
                { key: Row.Staging, value: false },
                { key: Row.MFA, value: false },
                { key: Row.Cookiebot, value: "vanaf € 15,- per maand" },
                { key: Row.CloudflarePro, value: "+ € 25,- per maand" },
            ],
        },
        {
            type: PackageType.Grow,
            rows: [
                { key: Row.Cost, value: "€ 50,-" },
                { key: Row.Backups, value: true },
                { key: Row.StorageCycle, value: true },
                { key: Row.PasswordProtection, value: true },
                { key: Row.UpdateFrequency, value: "Wekelijks" },
                { key: Row.VisualRegressionTesting, value: false },
                { key: Row.AutoBackup, value: true },
                { key: Row.ActivityLog, value: true },
                { key: Row.Caching, value: true },
                { key: Row.LazyLoading, value: true },
                { key: Row.DatabaseOptimalisation, value: true },
                { key: Row.ImageOptimalisation, value: true },
                { key: Row.LoginProtection, value: true },
                { key: Row.MalwareScanner, value: true },
                { key: Row.VulnerabilityScanner, value: true },
                { key: Row.ActivityLog, value: true },
                { key: Row.UptimeMonitoring, value: true },
                { key: Row.Staging, value: false },
                { key: Row.MFA, value: false },
                { key: Row.Cookiebot, value: "vanaf € 15,- per maand" },
                { key: Row.CloudflarePro, value: "+ € 25,- per maand" },
            ],
        },
        {
            type: PackageType.Scale,
            rows: [
                { key: Row.Cost, value: "€ 80,-" },
                { key: Row.Backups, value: true },
                { key: Row.StorageCycle, value: true },
                { key: Row.PasswordProtection, value: true },
                { key: Row.UpdateFrequency, value: "Dagelijks" },
                { key: Row.VisualRegressionTesting, value: true },
                { key: Row.AutoBackup, value: true },
                { key: Row.ActivityLog, value: true },
                { key: Row.Caching, value: true },
                { key: Row.LazyLoading, value: true },
                { key: Row.DatabaseOptimalisation, value: true },
                { key: Row.ImageOptimalisation, value: true },
                { key: Row.LoginProtection, value: true },
                { key: Row.MalwareScanner, value: true },
                { key: Row.VulnerabilityScanner, value: true },
                { key: Row.ActivityLog, value: true },
                { key: Row.UptimeMonitoring, value: true },
                { key: Row.Staging, value: true },
                { key: Row.MFA, value: true },
                { key: Row.Cookiebot, value: "vanaf € 15,- per maand" },
                { key: Row.CloudflarePro, value: "+ € 25,- per maand" },
            ],
        },
    ];

    return { rows, packages };
};
