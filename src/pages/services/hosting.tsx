import { GetServerSideProps, NextPage } from "next";
import React from "react";

const Hosting: NextPage = () => {
    return <></>;
};

export default Hosting;

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
    res.writeHead(301, {
        Location: "/services/shared-hosting",
    });

    res.end();
    return { props: {} };
};
