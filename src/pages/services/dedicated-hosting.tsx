import { NextPage } from "next";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import {
    TableRows,
    Package,
    PackageType,
    PackagesTable,
} from "../../components/packagesTable/packagesTable";
import React from "react";
import { HeadMeta } from "src/components/HeadMeta";
import { Hero } from "@components/hero";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";
import { ContactBanner } from "@components/contactBanner";
import { Button, ButtonType } from "@components/button";

const DedicatedHosting: NextPage = () => {
    useAppContext({ heroColor: "light" });

    const description =
        "Ontdek krachtige dedicated hostingoplossingen voor optimale prestaties en beveiliging. Krijg volledige controle over je serveromgeving en ondersteuning. Kies voor toegewijde hosting voor jouw bedrijf.";

    enum Row {
        Cost = "Maandelijks",
        Memory = "RAM Geheugen",
        vCPUs = "vCPUs",
        Storage = "NVMe SSD Opslag",
        MonthlyBandwidth = "Maandelijks Dataverkeer",
        SSL = "SSL Certificaat",
        Cloudflare = "Cloudflare",
        HSTS = "HSTS",
        SecurityHeaders = "Security Headers",
        EndpointMonitoring = "Endpoint Monitoring",
        Database = "Additionele Database",
        LoadBalancer = "Load Balancer",
        CDN = "Cloudfront CDN",
    }

    const rows: TableRows = [
        {
            key: Row.Cost,
        },
        { type: "heading", value: "Server" },
        {
            key: Row.Memory,
        },
        {
            key: Row.vCPUs,
        },
        {
            key: Row.Storage,
        },
        {
            key: Row.MonthlyBandwidth,
        },
        { type: "heading", value: "Beveiliging" },
        {
            key: Row.SSL,
        },
        {
            key: Row.Cloudflare,
        },
        {
            key: Row.HSTS,
        },
        {
            key: Row.SecurityHeaders,
        },
        {
            key: Row.EndpointMonitoring,
        },
        { type: "heading", value: "Extra's" },
        {
            key: Row.Database,
        },
        {
            key: Row.LoadBalancer,
        },
        {
            key: Row.CDN,
        },
    ];

    const packages: Package[] = [
        {
            type: PackageType.Basic,
            rows: [
                { key: Row.Cost, value: "€ 18,-" },
                { key: Row.Memory, value: "1 GB" },
                { key: Row.vCPUs, value: "2 vCPUs" },
                { key: Row.Storage, value: "40 GB" },
                { key: Row.MonthlyBandwidth, value: "2 TB" },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.SecurityHeaders, value: true },
                { key: Row.EndpointMonitoring, value: false },
                { key: Row.Database, value: "vanaf € 30,- per maand" },
                { key: Row.LoadBalancer, value: "€ 25,- per maand" },
                { key: Row.CDN, value: "vanaf € 20,- per maand" },
            ],
        },
        {
            type: PackageType.Business,
            rows: [
                { key: Row.Cost, value: "€ 30,-" },
                { key: Row.Memory, value: "2 GB" },
                { key: Row.vCPUs, value: "2 vCPUs" },
                { key: Row.Storage, value: "60 GB" },
                { key: Row.MonthlyBandwidth, value: "3 TB" },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.SecurityHeaders, value: true },
                { key: Row.EndpointMonitoring, value: false },
                { key: Row.Database, value: "vanaf € 30,- per maand" },
                { key: Row.LoadBalancer, value: "€ 25,- per maand" },
                { key: Row.CDN, value: "vanaf € 20,- per maand" },
            ],
        },
        {
            type: PackageType.Grow,
            rows: [
                { key: Row.Cost, value: "€ 195,-" },
                { key: Row.Memory, value: "16 GB" },
                { key: Row.vCPUs, value: "4 vCPUs" },
                { key: Row.Storage, value: "320 GB" },
                { key: Row.MonthlyBandwidth, value: "6 TB" },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.SecurityHeaders, value: true },
                { key: Row.EndpointMonitoring, value: true },
                { key: Row.Database, value: "vanaf € 30,- per maand" },
                { key: Row.LoadBalancer, value: "€ 25,- per maand" },
                { key: Row.CDN, value: "vanaf € 20,- per maand" },
            ],
        },
        {
            type: PackageType.Scale,
            rows: [
                { key: Row.Cost, value: "€ 350,-" },
                { key: Row.Memory, value: "32 GB" },
                { key: Row.vCPUs, value: "8 vCPUs" },
                { key: Row.Storage, value: "640 GB" },
                { key: Row.MonthlyBandwidth, value: "7 TB" },
                { key: Row.SSL, value: true },
                { key: Row.Cloudflare, value: true },
                { key: Row.HSTS, value: true },
                { key: Row.SecurityHeaders, value: true },
                { key: Row.EndpointMonitoring, value: true },
                { key: Row.Database, value: "vanaf € 30,- per maand" },
                { key: Row.LoadBalancer, value: "€ 25,- per maand" },
                { key: Row.CDN, value: "vanaf € 20,- per maand" },
            ],
        },
    ];

    return (
        <Layout overlay={false}>
            <HeadMeta
                title="Optimale prestaties en beveiliging"
                description={description}
                canonicalSlug={"/services/dedicated-hosting"}
                image={"default_og_image.jpg"}
            />
            <Hero
                title={"Dedicated Hosting"}
                subtitle={"Optimale prestaties en beveiliging."}
                image={{ src: "/dedicated-hosting.svg", alt: "dedicated hosting" }}
                body={
                    "Maximale performance en instelling voor jou bedrijf. Wij bieden de server voor jouw wensen."
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />
            <TitleTextRow title={"Waarom kiezen voor dedicated hosting bij Genips?"}>
                <Paragraph>
                    Onze dedicated hostingoplossingen zijn ontworpen om aan de unieke behoeften van
                    jouw bedrijf te voldoen. Met serverresources ben je verzekerd van optimale
                    prestaties, betrouwbaarheid en beveiliging. Of je nu een groeiende e-commerce
                    winkel beheert, complexe webtoepassingen host of gevoelige gegevens verwerkt,
                    onze toegewijde servers bieden de kracht en flexibiliteit die je nodig hebt.
                    <br />
                    <br />
                    We maken hiervoor gebruik van AWS LightSail servers in Duitsland. Wij beheren de
                    server en jij hebt er dus geen omkijken naar.
                    <br />
                    <br />
                </Paragraph>
                <Button
                    label="Op zoek naar (goedkopere) shared hosting?"
                    type={ButtonType.Secondary}
                    href="/services/shared-hosting"
                />
            </TitleTextRow>
            <PackagesTable packages={packages} rows={rows} />
            <ContactBanner
                heading="Klaar om jouw server te lanceren?"
                gaPageName="dedicated-hosting"
            />
        </Layout>
    );
};

export default DedicatedHosting;
