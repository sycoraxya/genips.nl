import { NextPage } from "next";
import React from "react";
import { Hero } from "@components/hero";
import { ContactBanner } from "@components/contactBanner";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";

const ConversieOptimalisatie: NextPage = () => {
    useAppContext({ heroColor: "white" });

    const description =
        "Je website heeft genoeg verkeer maar toch verkoop je niks? Dan is het tijd voor conversie optimalisatie. Genips helpt je graag verder!";

    return (
        <Layout overlayColor={"white"}>
            <HeadMeta
                title="Zorg voor meer klanten met conversie optimalisatie"
                description={description}
                canonicalSlug={"/services/conversie-optimalisatie"}
                image={"default_og_image.jpg"}
            />

            <Hero
                title={"Conversie Optimalisatie"}
                subtitle={"Meer, meer, meer!"}
                image={{ src: "/conversie-optimalisatie.svg", alt: "conversie optimalisatie" }}
                body={
                    "Je ontvangt genoeg verkeer op je website, je aanbod is groot, maar toch kopen bezoekers niets. Kijken, kijken, niet kopen. Hoe kan dat? Wij zorgen ervoor dat kijkers ook kopers worden!"
                }
                color="light"
                backButton={{
                    label: "services",
                    href: "/services",
                }}
            />
            <TitleTextRow title={"Onze technieken"}>
                <Paragraph>
                    Conversie optimalisatie houdt in dat je het aantal bezoekers dat daadwerkelijk
                    iets koopt op je website verhoogt. Helaas gaat dat niet zo simpel als het
                    klinkt. Stel jezelf de volgende vragen om te ontdekken waar het probleem zit:
                </Paragraph>
                <Paragraph>
                    <ul>
                        <li>Waarom koopt niet iedere bezoeker een product?</li>
                        <li>Waarom komen klanten niet vaker terug?</li>
                        <li>Tegen welke problemen lopen je klanten aan?</li>
                        <li> Hoe kun je klanten beter begeleiden in hun online aankoop?</li>
                    </ul>
                </Paragraph>
                <Paragraph>
                    Kun je antwoord geven op deze vragen? Waarschijnlijk nog niet. Laten we daarom
                    samen die antwoorden vinden en van je website een succes maken!
                </Paragraph>
            </TitleTextRow>
            <ContactBanner
                heading="Ook zin in meer digitaal?"
                gaPageName="conversie-optimalisatie"
            />
        </Layout>
    );
};

export default ConversieOptimalisatie;
