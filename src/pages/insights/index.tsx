import React from "react";
import { GetStaticProps, InferGetStaticPropsType, NextPage } from "next";
import { Layout } from "@components/Layout";
import { HeadMeta } from "src/components/HeadMeta";
import { Row } from "@components/row";
import { Col } from "@components/col";
import { Tag } from "@components/tag";
import { ArticleMeta, getFormattedDate, getHeroImage } from "@components/article";
import Link from "next/link";
import { ContactBanner } from "@components/contactBanner";
import { useAppContext } from "@hooks/useAppContext";
import { Hero } from "@components/hero";
import fs from "fs";
import styles from "@styles/insights/insights.module.scss";
import Image from "next/image";

interface Post extends Omit<ArticleMeta, "description" | "date" | "tags"> {
    slug: string;
    date: string;
}

// TODO: use typography titles & paragraphs

const Insights: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({ posts }) => {
    useAppContext();

    const description =
        "Wij hebben veel kennis van digitale producten en delen die kennis graag met jou. Lees onze inzichten en vind antwoord op al je vragen. ";

    return (
        <Layout>
            <HeadMeta
                title="Leer meer over digitale producten: lees onze inzichten"
                description={description}
                image={"insights_meta_image.jpg"}
                canonicalSlug={"/insights"}
            />
            <Hero
                title={"Inzichten"}
                subtitle={
                    "Profiteer van onze kennis: leer meer over de wereld van digitale producten. "
                }
                image={{ src: "insights.svg", alt: "Insights" }}
                containerClassName={styles.heroContainer}
            />
            <Row
                gutter={[
                    16,
                    {
                        md: 60,
                        xs: 50,
                        xxs: 50,
                    },
                ]}
                containerClassName={styles.articlesWrapper}
            >
                {posts
                    .sort((postOne, postTwo) => (postOne.date > postTwo.date ? -1 : 1))
                    .map((post, index) => {
                        const { staticImage, slug, title } = post;

                        return (
                            <Col span={12} md={{ span: 6 }} key={index}>
                                <Link href={`/insights/${slug}`} className={styles.imageWrapper}>

                                    <Image
                                        src={staticImage}
                                        layout={"fill"}
                                        placeholder={"blur"}
                                        alt=""
                                    />

                                </Link>
                                <h3 className={styles.title}>
                                    <Link href={`/insights/${slug}`}>
                                        {title}
                                    </Link>
                                </h3>
                            </Col>
                        );
                    })}
            </Row>
            <ContactBanner gaPageName={"insights"} />
        </Layout>
    );
};

export const getStaticProps: GetStaticProps<{ posts: Post[] }> = async (context) => {
    const files = (await fs.promises.readdir(`${process.cwd()}/src/pages/insights`)).filter(
        (name) => name !== "index.tsx",
    );

    const posts: Post[] = [];
    for (const file of files) {
        const { meta } = (await import(`./${file}`)) as { meta: ArticleMeta };

        posts.push({
            title: meta.title,
            date: meta.date.toString(),
            slug: file.replace(".mdx", ""),
            staticImage: meta.staticImage,
        });
    }

    return {
        props: {
            posts,
        },
    };
};

export default Insights;
