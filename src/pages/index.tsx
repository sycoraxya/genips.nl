import { NextPage } from "next";
import React from "react";
import { PortfolioStudioSchneemann } from "../components/portfolio/PortfolioStudioSchneemann";
import { HomeHero } from "@components/home/homeHero";
import { Layout } from "@components/Layout";
import { useAppContext } from "@hooks/useAppContext";
import { HeadMeta } from "src/components/HeadMeta";
import { HomeSteps } from "@components/home/homeSteps";
import { Reviews } from "@components/home/reviews";
import { PortfolioAddVue } from "src/components/portfolio/PortfolioAddVue";
import { PortfolioHaasF1 } from "src/components/portfolio/PortfolioHaasF1";
import { PortfolioLuna } from "src/components/portfolio/PortfolioLuna";
import { PortfolioAntilopen } from "src/components/portfolio/PortfolioAntilopen";
import { PortfolioSVM } from "src/components/portfolio/PortfolioSVM";
import { PortfolioRetourkoop } from "src/components/portfolio/PortfolioRetourkoop";
import { PortfolioPromentum } from "src/components/portfolio/PortfolioPromentum";
import { PortfolioNewcold } from "src/components/portfolio/PortfolioNewcold";
import { PortfolioGlobos } from "src/components/portfolio/PortfolioGlobos";
import { PortfolioSannewevers } from "src/components/portfolio/PortfolioSannewevers";
import { PortfolioVerne } from "src/components/portfolio/PortfolioVerne";
import { PortfolioEye } from "src/components/portfolio/PortfolioEye";
import { PortfolioPhonecam } from "src/components/portfolio/PortfolioPhonecam";
import { PortfolioBijzonderecamping } from "src/components/portfolio/PortfolioBijzonderecamping";
import { PortfolioBasicproducts } from "src/components/portfolio/PortfolioBasicproducts";

const Home: NextPage = () => {
    useAppContext();

    const description = "Jouw digitale transformatie begint hier.";

    return (
        <Layout>
            <HeadMeta
                title="Digital Product Agency"
                description={description}
                canonicalSlug={""}
                image="/default_og_image.jpg"
            />
            <HomeHero />
            <HomeSteps />
            <Reviews />

            <PortfolioRetourkoop />
            <PortfolioSannewevers imagePositionOverride="left" />
            <PortfolioSVM imagePositionOverride="right" />

            <PortfolioHaasF1 imagePositionOverride="left" />
            <PortfolioAntilopen />
            <PortfolioEye imagePositionOverride="left" />

            <PortfolioLuna />
            <PortfolioStudioSchneemann />
            <PortfolioPhonecam />

            <PortfolioGlobos imagePositionOverride="left" />
            <PortfolioBijzonderecamping />
            <PortfolioBasicproducts />

            <PortfolioNewcold imagePositionOverride="right" />
            <PortfolioPromentum />
            <PortfolioVerne />

            <PortfolioAddVue />
        </Layout>
    );
};

export default Home;
