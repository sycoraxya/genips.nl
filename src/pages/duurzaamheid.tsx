import React from "react";
import { NextPage } from "next";
import { Layout } from "@components/Layout";
import { Hero } from "@components/hero";
import { HeadMeta } from "src/components/HeadMeta";
import { useAppContext } from "@hooks/useAppContext";
import { TreeCounter } from "@components/treeCounter";
import { TitleTextRow } from "@components/titleTextRow";
import { Paragraph } from "@components/typography";

const Sustainability: NextPage = () => {
    useAppContext({ heroColor: "green" });
    const description =
        "Wij maken digitale producten en werken daarnaast graag aan een beter klimaat. Daarom plant Genips voor elke klant een boom.";

    return (
        <Layout>
            <HeadMeta
                title="Jouw duurzame partner in digitale producten"
                description={description}
                image={"default_og_image.jpg"}
                canonicalSlug={"/duurzaamheid"}
            />
            <Hero
                title={"Duurzaamheid"}
                subtitle={"Genips werkt aan een beter klimaat."}
                body={
                    "Als we allemaal ons steentje bijdragen aan een groene en duurzame wereld, dan kunnen we een hoop bereiken! Daarom planten wij namens al onze klanten een boom."
                }
                color="green"
            />
            <TitleTextRow title={"Iedere klant = één boom geplant"}>
                <Paragraph>
                    Onze natuur heeft het helaas niet makkelijk. Elke minuut worden 40 voetbalvelden
                    aan bomen gekapt. Wij vinden dat de vernietiging van deze belangrijke
                    ecosystemen moet stoppen.
                </Paragraph>
                <Paragraph>
                    Daarom plant Genips in samenwerking met Trees for Kenya voor iedere klant één
                    boom. Dit doen we samen met ons product{" "}
                    <a href="https://sportclubvrijwilligersmanagement.nl" target="_blank">
                        Sportclub Vrijwilligers Management
                    </a>
                    . Zo helpen we nieuwe ecosystemen te realiseren en de mensen in Kenya te helpen
                    aan het opbouwen van een duurzaam verdienmodel. Samen met jou dragen we bij aan
                    een leefbare planeet.
                </Paragraph>
            </TitleTextRow>
            <TitleTextRow title={"Groene energie"} titleOrder={1}>
                <Paragraph>
                    De servers van Genips staan in een datacenter in Amsterdam en Frankfurt. Deze
                    datacenters gebruiken 100% herbruikbare groene energie. De servers worden
                    gekoeld met water en restwarmte wordt doorgevoerd naar lokale scholen en
                    bedrijven. Het water wordt na het afkoelen opnieuw gebruikt om de servers te
                    koelen. Groener kan het bijna niet!
                </Paragraph>
            </TitleTextRow>
            <TitleTextRow title={"Duurzaam onderweg"}>
                <Paragraph>
                    Wij hebben geen kantoren, hierdoor besparen we reistijd, maar nog belangrijker:
                    onnodige uitstoot door het reizen van en naar kantoor. Als we bij jou op bezoek
                    komen doen we altijd per trein, fiets, te voet of met een elektrische auto. En
                    komen we bij je op bezoek? Onze elektrische auto's laden we op met groene,
                    duurzame, stroom met vandebron.
                </Paragraph>
            </TitleTextRow>
            <TreeCounter />
        </Layout>
    );
};

export default Sustainability;
