import React from "react";
import { NextPage } from "next";
import { Layout } from "../components/layout";
import { Hero } from "@components/hero";
import { HeadMeta } from "../components/HeadMeta";

export const FourOhFour: NextPage = () => {
    return (
        <Layout>
            <HeadMeta
                title="404"
                description={"Oeps, hier is iets misgegaan!"}
                canonicalSlug={"/404"}
                image="default_og_image.jpg"
            />
            <Hero
                image={{ src: "/error.svg", alt: "404 not found" }}
                title={"Oeps, hier is iets misgegaan"}
                body={
                    "We zoeken het tot op de bodem uit en helpen je graag de weg weer terug te vinden."
                }
                button={{
                    label: "Terug naar de homepage",
                    href: "/",
                }}
            />
        </Layout>
    );
};

export default FourOhFour;
