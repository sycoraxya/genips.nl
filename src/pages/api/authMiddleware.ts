import { NextApiRequest, NextApiResponse } from "next";
import initMiddleware from "./initMiddleware";

export const authMiddleware = initMiddleware(
    (req: NextApiRequest, res: NextApiResponse, callback) => {
        if (!req.cookies.genips) {
            res.status(401);
            res.send("");
            return callback(new Error("401 Unauthorized"));
        }
        const buffer = Buffer.from(req.cookies.genips, "base64");

        const apiSecret = buffer.toString("ascii");
        if (apiSecret === process.env.API_SECRET) {
            return callback();
        } else {
            res.status(401);
            res.send("");
            return callback(new Error("401 Unauthorized"));
        }
    },
);
