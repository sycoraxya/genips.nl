import { SentMessageInfo } from "nodemailer";
import { ContactBody } from "../../../components/contactForm";
import { transporter } from "./transporter";

export const sendUserMail = async (
    body: ContactBody,
    subject: string,
): Promise<SentMessageInfo> => {
    return transporter.sendMail({
        from: "no-reply@genips.nl",
        to: body.email,
        subject: `Bedankt voor je contactverzoek op Genips.nl`,
        text: `Hoi ${body.fullName},

Bedankt voor je contactverzoek, we nemen zo snel mogelijk contact met je op.

Dit zijn de gegevens die je naar ons meegestuurd hebt:<br/>
Naam: ${body.fullName}
Bedrijf: ${body.companyName}
Email: ${body.email}
Onderwerp: ${subject}
Bericht: ${body.message}`,
        html: `Hoi ${body.fullName},<br/><br/>
        Bedankt voor je contactverzoek, we nemen zo snel mogelijk contact met je op.<br/>
        <br/>
        Dit zijn de gegevens die je naar ons meegestuurd hebt:<br/>
        Naam: ${body.fullName}<br/>
        Bedrijf: ${body.companyName}<br/>
        Email: ${body.email}<br/>
        Onderwerp: ${subject}<br/>
        Bericht: <pre>${body.message}</pre>`,
    });
};
