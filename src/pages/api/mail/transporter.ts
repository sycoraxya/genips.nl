import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";

let transporter: Mail;

if (process.env.NODE_ENV === "development") {
    transporter = nodemailer.createTransport({
        port: process.env.MAIL_PORT,
        host: process.env.MAIL_HOST,
    });
} else {
    transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASS,
        },
    });
}

export { transporter };
