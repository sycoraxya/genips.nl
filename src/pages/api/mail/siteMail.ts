import { SentMessageInfo } from "nodemailer";
import { ContactBody } from "../../../components/contactForm";
import { transporter } from "./transporter";

export const sendSiteMail = async (
    body: ContactBody,
    subject: string,
): Promise<SentMessageInfo> => {
    return transporter.sendMail({
        from: "no-reply@genips.nl",
        to: "hallo@genips.nl",
        subject: `Genips.nl contactverzoek van ${body.fullName}`,
        text: `Contactverzoek van ${body.fullName} over ${subject}
Mijn naam is ${body.fullName} en ik werk bij ${body.companyName}.
Ik ben op zoek naar informatie over ${subject}.
Je kunt me bereiken via ${body.email} zodat we kunnen praten.

Optioneel, wil ik dit nog even kwijt:
${body.message}`,
        html: `<h3>Contactverzoek van ${body.fullName} over ${subject}</h3><br/>
        Mijn naam is <b>${body.fullName}</b> en ik werk bij <b>${body.companyName}</b>.<br/>
        Ik ben op zoek naar informatie over <b>${subject}</b>. <br/>
        Je kunt me bereiken via <b>${body.email}</b> zodat we kunnen praten.<br/>
        <br/>
        Optioneel, wil ik dit nog even kwijt: <br/>
        <pre>${body.message}</pre>`,
    });
};
