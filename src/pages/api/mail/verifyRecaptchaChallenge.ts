export const verifyRecaptchaChallenge = async (
    recaptchaResponseToken: string,
): Promise<{ success: boolean; challenge_ts: string; hostname: string; score: number }> => {
    const googleUrl = new URL("https://www.google.com/recaptcha/api/siteverify");
    googleUrl.searchParams.append("secret", process.env.RECAPTCHA_SECRET);
    googleUrl.searchParams.append("response", recaptchaResponseToken);

    const captchaPost = await fetch(googleUrl.toString(), {
        method: "POST",
    });

    return captchaPost.json();
};
