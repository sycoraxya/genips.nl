import { NextApiRequest, NextApiResponse } from "next";
import { ContactBody, Subject } from "../../../components/contactForm";
import { authMiddleware } from "../authMiddleware";
import { sendSiteMail } from "./siteMail";
import { sendUserMail } from "./userMail";
import { verifyRecaptchaChallenge } from "./verifyRecaptchaChallenge";

export interface MailResponse {
    success: boolean;
    error?: string;
}

const handle = async (req: NextApiRequest, res: NextApiResponse<MailResponse>) => {
    await authMiddleware(req, res);

    if (req.method !== "POST") {
        res.setHeader("Allow", ["POST"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        return;
    }

    const body = JSON.parse(req.body) as ContactBody;
    const subject = Subject[body.subject];

    if (!body.recaptchaResponseToken) {
        console.error("recaptcha response token is missing");
        res.json({ success: false });

        return;
    }
    const recaptchaResponse = await verifyRecaptchaChallenge(body.recaptchaResponseToken);

    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");

    // <= 0.5 is probably a bot, send an error back to the form and let them send a normal email instead
    if (recaptchaResponse.score <= 0.5) {
        console.error(
            `A bot tried to send an email \n`,
            JSON.stringify({ recaptchaResponse }, null, 2),
        );

        res.json({ success: false });

        return;
    }

    try {
        const mailStats = await sendSiteMail(body, subject);

        console.info(mailStats);
    } catch (e) {
        res.json({ success: false, error: e.response || e.message });
        return;
    }

    // This shouldn't throw an error because it's less critical
    try {
        await sendUserMail(body, subject);
    } catch (e) {}

    res.json({ success: true });
};

export default handle;
