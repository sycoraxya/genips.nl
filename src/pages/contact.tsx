import { GetServerSideProps, NextPage } from "next";
import { useAppContext } from "../hooks/useAppContext";
import { ContactBanner } from "@components/contactBanner";
import React from "react";
import { HeadMeta } from "../components/HeadMeta";
import { ContactForm } from "@components/contactForm";
import cookie from "cookie";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
import { Layout } from "@components/Layout";
import { Hero } from "@components/hero";

const Contact: NextPage = () => {
    useAppContext();

    const description =
        "Heb je een vette uitdaging waarvan onze hersenen gaan kraken? Mooi zo! Neem dan gauw contact met ons op.";

    return (
        <GoogleReCaptchaProvider
            reCaptchaKey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
            language="nl"
        >
            <Layout>
                <HeadMeta
                    title="Contact"
                    description={description}
                    image={"default_og_image.jpg"}
                    canonicalSlug={"/contact"}
                />
                <Hero
                    title={"Waar ben je naar op zoek?"}
                    subtitle={"Laat het ons weten."}
                    image={{ src: "contact.svg", alt: "Contact" }}
                    body={
                        "Leuk dat je met ons wilt samenwerken! Laat ons gauw iets weten. Dan kunnen we samen aan de slag gaan met jouw project."
                    }
                />
                <ContactForm />
                <ContactBanner
                    gaPageName="Contact"
                    heading="Geen fan van contactformulieren? Mail ons:"
                />
            </Layout>
        </GoogleReCaptchaProvider>
    );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
    const buffer = Buffer.from(process.env.API_SECRET);
    context.res.setHeader(
        "Set-Cookie",
        cookie.serialize("genips", buffer.toString("base64"), {
            httpOnly: true,
            sameSite: true,
            secure: process.env.NODE_ENV === "development" ? false : true,
            path: "/",
        }),
    );

    return {
        props: {},
    };
};

export default Contact;
