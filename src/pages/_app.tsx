import React from "react";
import { AppProps } from "next/app";
import "../styles/global.scss";
import { Header } from "../components/header/header";
import { Footer } from "@components/footer";
import Router from "next/router";
import NProgress from "nprogress";
import { AppContextProvider } from "../components/appContext";
import { AnimatePresence, domAnimation, LazyMotion, m, motion } from "framer-motion";
import { HeaderContextProvider } from "../components/header/context";

Router.events.on("routeChangeStart", (url) => {
    NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const animation = {
    name: "Fade Back",
    variants: {
        initial: {
            opacity: 0,
        },
        animate: {
            opacity: 1,
        },
        exit: {
            opacity: 0,
        },
    },
    transition: {
        duration: 0.2,
    },
};

const handleRouteChange = () => {
    const elements = document.querySelectorAll('style[media="x"]');
    elements.forEach((elem) => elem.removeAttribute("media"));
    setTimeout(() => {
        elements.forEach((elem) => elem.remove());
    }, 0.3 * 1000);
};

Router.events.on("routeChangeStart", handleRouteChange);
Router.events.on("routeChangeComplete", handleRouteChange);

const MyApp = ({ Component, pageProps, router }: AppProps) => {
    return (
        <motion.div className="page">
            <AppContextProvider>
                <HeaderContextProvider>
                    <Header />

                    <LazyMotion features={domAnimation}>
                        <AnimatePresence mode="wait">
                            <m.div
                                key={router.route.concat(animation.name)}
                                className="page-wrap"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={animation.variants}
                                transition={animation.transition}
                            >
                                <Component {...pageProps} key={router.route} />
                            </m.div>
                        </AnimatePresence>
                    </LazyMotion>

                    <Footer />
                </HeaderContextProvider>
            </AppContextProvider>
        </motion.div>
    );
};

export default MyApp;
