import { NextPage, GetServerSideProps } from "next";
import { statSync, readdirSync } from "fs";
import { resolve, parse } from "path";

const Sitemap: NextPage = () => {
    return <div></div>;
};

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
    const ignore = [
        "_app.tsx",
        "_document.tsx",
        "sitemap.xml.tsx",
        "index.tsx",
        "api",
        "mailhosting.tsx",
        "404.tsx",
    ];
    const pagesDir = readdirSync(resolve("src", "pages"));
    const servicesPages = readdirSync(resolve("src", "pages", "services"));
    const insightsPages = readdirSync(resolve("src", "pages", "insights"));
    let xml = `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">`;
    const indexStats = statSync(resolve("src", "pages", "index.tsx"));

    xml += `
        <url>
            <loc>https://genips.nl/</loc>
            <lastmod>${new Date(indexStats.mtimeMs).toISOString()}</lastmod>
            <priority>1</priority>
        </url>
        `;

    pagesDir.map((fileName) => {
        if (ignore.includes(fileName)) {
            return null;
        }

        const fileStats = statSync(resolve("src", "pages", fileName));
        const name = parse(fileName).name;

        xml += `
        <url>
            <loc>https://genips.nl/${name}</loc>
            <lastmod>${new Date(fileStats.mtimeMs).toISOString()}</lastmod>
            <priority>${name === "algemene-voorwaarden" ? "0.5" : "1"}</priority>
        </url>
        `;
    });

    servicesPages.map((fileName) => {
        if (ignore.includes(fileName)) {
            return null;
        }

        const fileStats = statSync(resolve("src", "pages", "services", fileName));

        xml += `
        <url>
            <loc>https://genips.nl/services/${parse(fileName).name}</loc>
            <lastmod>${new Date(fileStats.mtimeMs).toISOString()}</lastmod>
            <priority>1</priority>
        </url>
        `;
    });

    insightsPages.map((fileName) => {
        if (ignore.includes(fileName)) {
            return null;
        }

        const fileStats = statSync(resolve("src", "pages", "insights", fileName));

        xml += `
        <url>
            <loc>https://genips.nl/insights/${parse(fileName).name}</loc>
            <lastmod>${new Date(fileStats.mtimeMs).toISOString()}</lastmod>
            <priority>1</priority>
        </url>
        `;
    });

    xml += "</urlset>";

    res.setHeader("Content-Type", "text/xml");

    res.write(xml);
    res.end();

    return { props: {} };
};

export default Sitemap;
