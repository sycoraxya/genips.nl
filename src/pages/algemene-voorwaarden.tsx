import { NextPage, GetServerSideProps } from "next";

const TermsAndConditions: NextPage = () => {
    return <div></div>;
};

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
    res.writeHead(301, {
        Location: "/Algemene Voorwaarden 2025.pdf",
    });

    res.end();
    return { props: {} };
};

export default TermsAndConditions;
