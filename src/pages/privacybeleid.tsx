import { Col, Row } from "@components/grid";
import { Hero } from "@components/hero";
import { Layout } from "@components/Layout";
import { Paragraph } from "@components/typography";
import { useAppContext } from "@hooks/useAppContext";
import { NextPage } from "next";
import React from "react";
import { HeadMeta } from "src/components/HeadMeta";
import sectionStyles from "@styles/section/section.module.scss";
import { ContactBanner } from "@components/contactBanner";

const PrivacyPolicy: NextPage = () => {
    useAppContext();

    const description =
        "Natuurlijk gaan wij netjes om met jouw data. Daarom hebben we alle details vastgelegd in onze privacy- en cookieverklaring.";

    return (
        <Layout>
            <HeadMeta
                title="Privacyverklaring: hoe wij omgaan met jouw data"
                description={description}
                image={"default_og_image.jpg"}
                canonicalSlug={"/privacybeleid"}
            />
            <Hero
                title={"Privacy- en Cookieverklaring"}
                subtitle={"Natuurlijk gaan we netjes om met jouw data."}
                image={{ src: "privacy.svg", alt: "Privacy- en Cookieverklaring" }}
            />
            <Row containerClassName={sectionStyles.paddingY}>
                <Col span={12}>
                    <Paragraph>
                        Jouw privacy is voor Genips van groot belang. Wij houden ons dan ook aan de
                        relevante wet- en regelgeving over privacy, waaronder de Algemene
                        Verordening Gegevensbescherming (verder: AVG). Dit betekent dat wij:
                        <ul>
                            <li>
                                <b>Onze doeleinden duidelijk vastleggen</b>, voordat wij jouw
                                persoonlijke gegevens verwerken, via deze privacyverklaring;
                            </li>
                            <li>
                                <b>Zo min mogelijk persoonlijke gegevens opslaan</b> en enkel de
                                gegevens die nodig zijn voor onze doeleinden;
                            </li>
                            <li>
                                <b>Expliciet toestemming vragen</b> voor de verwerking van jouw
                                persoonlijke gegevens, mocht toestemming verplicht zijn;
                            </li>
                            <li>
                                <b>Benodigde beveiligingsmaatregelen treffen</b> om jouw
                                persoonlijke gegevens te beschermen. Wij leggen deze verplichtingen
                                ook op aan partijen die persoonsgegevens voor ons verwerken;
                            </li>
                            <li>
                                <b>Jouw rechten respecteren</b>, zoals het recht op inzage,
                                correctie of verwijdering van jouw bij ons verwerkte
                                persoonsgegevens.
                            </li>
                        </ul>
                    </Paragraph>
                    <Paragraph>
                        Jouw gegevens zijn veilig bij ons en wij zullen deze gegevens altijd netjes
                        gebruiken. In deze privacyverklaring leggen we uit wat wij bij het bedrijf
                        Genips allemaal doen met informatie die wij over jou te weten komen.
                    </Paragraph>
                    <Paragraph>
                        Als je vragen hebt of wilt weten wat wij precies van jou bijhouden, neem dan
                        contact op met Genips.
                    </Paragraph>
                    <Paragraph>
                        <b>Toegang portaal</b>
                        <br />
                        Met ons portaal krijg je toegang tot een beheeromgeving waar je zelf dingen
                        kunt instellen, opgeven en wijzigen voor het bedrijf. Wij houden bij wat je
                        gedaan hebt en wanneer, zodat daar bewijs van is. Hiervoor gebruiken wij
                        jouw:
                        <ul>
                            <li>NAW-gegevens</li>
                            <li>Telefoonnummer</li>
                            <li>E-mailadres</li>
                            <li>Nickname</li>
                        </ul>
                    </Paragraph>
                    <Paragraph>
                        Deze hebben wij nodig vanwege het contract dat we met je sluiten. Wij
                        bewaren deze informatie, tot drie maanden na het einde van de
                        dienstverlening aan jou.
                    </Paragraph>
                    <Paragraph>
                        <b>Contactformulier</b>
                        <br />
                        Met het contactformulier kun je ons vragen stellen of aanvragen doen.
                        Hiervoor gebruiken wij jouw:
                        <ul>
                            <li>NAW-gegevens</li>
                            <li>E-mailadres</li>
                        </ul>
                    </Paragraph>
                    <Paragraph>
                        Hier hebben wij een legitiem belang bij. Wij bewaren deze informatie, totdat
                        we zeker weten dat je tevreden bent met onze reactie.
                    </Paragraph>
                    <Paragraph>
                        <b>Verstrekking aan andere bedrijven of instellingen</b>
                        <br />
                        Met uitzondering van de partijen die nodig zijn om de hierboven genoemde
                        diensten te leveren, geven wij jouw persoonsgegevens onder geen voorwaarde
                        aan andere bedrijven of instellingen, behalve als wij dat wettelijk
                        verplicht zijn (bijvoorbeeld als de politie dat eist bij een vermoeden van
                        een misdrijf).
                    </Paragraph>
                    <Paragraph>
                        <b>Statistieken</b>
                        <br />
                        Wij houden statistieken bij over het gebruik van ons bedrijf.
                    </Paragraph>
                    <Paragraph>
                        <b>Cookies</b>
                        <br />
                        Ons bedrijf gebruikt cookies. Cookies zijn kleine bestandjes waar we
                        informatie in kunnen opslaan, zodat je die niet steeds hoeft in te vullen.
                        Maar wij kunnen er ook mee zien dat je ons weer bezoekt.
                    </Paragraph>
                    <Paragraph>
                        Je kunt via jouw browser het plaatsen van cookies uitschakelen, maar sommige
                        dingen van ons bedrijf werken dan niet goed meer.
                    </Paragraph>
                    <Paragraph>
                        Met andere bedrijven die cookies plaatsen hebben wij afspraken gemaakt over
                        het gebruik van de cookies. Toch hebben wij geen volledige controle op wat
                        zij zelf met de cookies doen. Lees dus ook hun privacyverklaringen.
                    </Paragraph>
                    <Paragraph>
                        <b>Beveiliging</b>
                        <br />
                        Beveiliging van persoonsgegevens is voor ons van groot belang. Om jouw
                        privacy te beschermen, nemen wij de volgende maatregelen:
                        <ul>
                            <li>
                                Wij maken gebruik van beveiligde verbindingen (Secure Sockets Layer
                                of SSL) waarmee alle informatie tussen jou en onze website wordt
                                afgeschermd wanneer jij jouw persoonsgegevens invoert.
                            </li>
                        </ul>
                    </Paragraph>
                    <Paragraph>
                        <b>Wijzigingen in deze privacyverklaring</b>
                        <br />
                        Wanneer ons bedrijf wijzigt, moeten wij natuurlijk ook de privacyverklaring
                        aanpassen. Let dus altijd op de datum hierboven en kijk regelmatig of er
                        nieuwe versies zijn. Wij zullen ons best doen wijzigingen aan je door te
                        geven.
                    </Paragraph>
                    <Paragraph>
                        <b>Jouw rechten</b>
                        <br />
                        Als je vragen hebt of wilt weten welke persoonsgegevens wij van jou hebben,
                        kun je altijd contact met ons opnemen. Zie de contactgegevens hieronder.
                    </Paragraph>
                    <Paragraph>
                        Je hebt de volgende rechten:
                        <ul>
                            <li>
                                Uitleg krijgen over welke persoonsgegevens wij hebben en wat wij
                                daarmee doen;
                            </li>
                            <li>Inzage in de precieze persoonsgegevens die wij hebben;</li>
                            <li>Het laten corrigeren van fouten;</li>
                            <li>Het laten verwijderen van verouderde persoonsgegevens;</li>
                            <li>
                                Het laten overdragen van persoonsgegevens aan een andere partij;
                            </li>
                            <li>Intrekken van toestemming;</li>
                            <li>Een bepaalde verwerking beperken;</li>
                            <li>Bezwaar maken tegen een bepaald gebruik.</li>
                        </ul>
                    </Paragraph>
                    <Paragraph>
                        Let op dat je altijd duidelijk aangeeft wie je bent, zodat we zeker weten
                        dat wij geen gegevens van de verkeerde persoon aanpassen of verwijderen.
                    </Paragraph>
                    <Paragraph>
                        Wij zullen in principe binnen een maand aan jouw verzoek voldoen. Deze
                        termijn kan echter worden verlengd om redenen die verband houden met de
                        specifieke rechten van betrokkenen of de complexiteit van het verzoek. Als
                        wij deze termijn verlengen, zullen wij je daarvan tijdig op de hoogte
                        stellen.
                    </Paragraph>
                    <Paragraph>
                        <b>Klacht indienen</b>
                        <br />
                        Als je een klacht in wil dienen over het gebruik van jouw persoonsgegevens
                        kun je een e-mail sturen naar hallo@genips.nl. Wij pakken elke klacht intern
                        op en communiceren dit verder met je.
                    </Paragraph>
                    <Paragraph>
                        Als je vindt dat wij je niet op de juiste manier helpen, dan heb je het
                        recht om een klacht in te dienen bij de toezichthouder. Deze heet de
                        Autoriteit Persoonsgegevens.
                    </Paragraph>
                    <Paragraph>
                        <b>Contactgegevens</b>
                        <br />
                        Genips B.V.
                        <br />
                        Meidoornhof 19
                        <br />
                        3831 XR Leusden
                        <br />
                        E-mailadres: hallo@genips.nl
                        <br />
                        Telefoon: +31 6 254 630 10
                        <br />
                        KVK: 92267114
                    </Paragraph>
                </Col>
            </Row>
            <ContactBanner
                gaPageName="Privacybeleid"
                heading="Vragen over onze Privacy- en Cookieverklaring?"
            />
        </Layout>
    );
};

export default PrivacyPolicy;
