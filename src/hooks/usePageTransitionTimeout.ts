import { useRouter } from "next/router";
import { DependencyList, useEffect } from "react";

export const usePageTransitionTimeout = (
    handler: (...evts: any[]) => void,
    deps: DependencyList = [],
) => {
    const router = useRouter();

    useEffect(() => {
        // Should be about the overlay's incoming speed + 100ms for good measure
        const timedOut = () => {
            setTimeout(() => {
                handler();
            }, 200);
        };

        router.events.on("routeChangeComplete", timedOut);

        return () => {
            router.events.off("routeChangeComplete", timedOut);
        };
    }, deps);
};
