import { useContext, useEffect } from "react";
import { AppContext, AppContextProps } from "../components/appContext";

interface UseAppContextArgs {
    heroColor?: AppContextProps["heroColor"];
}

// Should only be used by pages
export const useAppContext = ({ heroColor }: UseAppContextArgs = {}) => {
    const { heroColor: contextHeroColor, setHeroColor } = useContext(AppContext);

    useEffect(() => {
        setHeroColor(heroColor || "dark");
    });

    return {
        contextHeroColor,
    };
};
