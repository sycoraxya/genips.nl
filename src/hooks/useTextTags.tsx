import React from "react";
import { TextTagProps } from "src/interfaces/text-tag-props.interface";

export const useTextTags = (textTagProps: TextTagProps, _content: React.ReactNode) => {
    const { del, mark, italic, underline, code } = textTagProps;

    let content = _content;

    if (del) {
        content = <del>{content}</del>;
    }

    if (mark) {
        content = <mark>{content}</mark>;
    }

    if (italic) {
        content = <i>{content}</i>;
    }

    if (underline) {
        content = <u>{content}</u>;
    }

    if (code) {
        content = <code>{content}</code>;
    }

    return content;
};
