import React, { FunctionComponent, HTMLAttributes } from "react";
import { Row } from "@components/row";

import styles from "@styles/contactBanner/contactBanner.module.scss";
import { Col } from "@components/col";
import { Title } from "@components/typography";

interface ContactBannerProps extends HTMLAttributes<HTMLDivElement> {
    heading?: string;
    gaPageName?: string;
}

export const ContactBanner: FunctionComponent<React.PropsWithChildren<ContactBannerProps>> = ({
    heading,
    gaPageName,
    children,
    ...props
}) => {
    return (
        <Row {...props} className={styles.row} justify={"center"} overflow>
            <Col>
                <Title visualLevel={4} className={styles.heading} div>
                    {heading || "Stuur ons een bericht"}
                </Title>
                <div className={styles.email}>
                    <a
                        href="mailto:hallo@genips.nl"
                        onClick={() =>
                            //@ts-ignore
                            window.gtag("event", "Click", {
                                category: "Contact",
                                label: `Clicked email address ${gaPageName || "about us"} page`,
                            })
                        }
                        className={styles.emailUrl}
                    >
                        hallo@genips.nl
                    </a>
                </div>
            </Col>
        </Row>
    );
};
