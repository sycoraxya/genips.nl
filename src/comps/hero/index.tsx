import { Col, Row } from "@components/grid";
import React, { FunctionComponent, MutableRefObject } from "react";
import styles from "@styles/hero/hero.module.scss";
import typographyStyles from "@styles/typography/typography.module.scss";
import classNames from "classnames/bind";
import { Paragraph, Title } from "@components/typography";
import { useAppContext } from "@hooks/useAppContext";
import Image from "next/image";
import { Button, ButtonType } from "@components/button";
import { imageLoader } from "@utils/imageLoader";
import { UrlObject } from "url";
import { ImageProp } from "@interfaces/image-prop.interface";

const cx = classNames.bind(styles);

interface HeroProps {
    title?: string;
    subtitle?: string;
    body?: React.ReactNode;
    button?: {
        label: string;
        onClick?: () => {};
        href?: string | UrlObject;
    };
    image?: ImageProp;
    backButton?: {
        href: string;
        label: string;

        onClick?: () => {};
    };
    containerClassName?: string;
    color?: "green" | "light" | "dark";
}

export const Hero: FunctionComponent<React.PropsWithChildren<HeroProps>> = ({
    title,
    subtitle,
    body,
    image,
    button,
    backButton,
    containerClassName,
    color = "dark",
}) => {
    const { contextHeroColor } = useAppContext();
    const containerClasses = cx(
        "root",
        { [color]: color !== contextHeroColor, [contextHeroColor]: contextHeroColor === color },
        containerClassName,
        { empty: !title && !image },
    );
    const subtitleClasses = cx(typographyStyles.faded, styles.subtitle);

    return (
        <Row
            containerClassName={containerClasses}
            gutter={80}
            containerAttributes={{ "data-color": contextHeroColor }}
        >
            {title && (
                <Col
                    span={12}
                    order={1}
                    content={{ align: { vertical: "center" } }}
                    lg={{ span: 6, order: 0 }}
                >
                    {backButton && (
                        <Button
                            type={ButtonType.Secondary}
                            {...backButton}
                            className={styles.backButton}
                            iconPosition="start"
                        />
                    )}
                    <Title visualLevel={2} className={styles.title}>
                        {title}
                    </Title>
                    {subtitle && (
                        <Title level={2} visualLevel={3} className={subtitleClasses}>
                            {subtitle}
                        </Title>
                    )}
                    {body && (
                        <Paragraph size={"medium"} className={styles.body}>
                            {body}
                        </Paragraph>
                    )}
                    {button && (
                        <Button type={ButtonType.Secondary} {...button} className={styles.button} />
                    )}
                </Col>
            )}
            {image && (
                <Col span={12} lg={6}>
                    <Image
                        src={image.src}
                        alt={image.alt}
                        width={800}
                        height={600}
                        className={styles.image}
                    />
                </Col>
            )}
        </Row>
    );
};
