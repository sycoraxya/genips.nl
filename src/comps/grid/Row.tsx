import React, {
    CSSProperties,
    FunctionComponent,
    HTMLAttributes,
    useEffect,
    useState,
} from "react";
import { tuple } from "@utils/type";
import classNames from "classnames/bind";
import RowContext from "@components/grid/RowContext";
import styles from "@styles/grid/grid.module.scss";
import { classify } from "@utils/classify";
import ResponsiveObserve, {
    Breakpoint,
    ScreenMap,
    responsiveArray,
} from "@utils/responsiveObserve";
import themeSettings from "@theme-settings.module.scss";

const cx = classNames.bind(styles);

export type Gutter = number | Partial<Record<Breakpoint, number>>;

const maxWidth = 1680;
const RowAligns = tuple("top", "middle", "bottom", "stretch");
const RowJustify = tuple("start", "end", "center", "space-around", "spaceBetween");

interface RowProps extends HTMLAttributes<HTMLDivElement> {
    gutter?: Gutter | [Gutter, Gutter];
    align?: typeof RowAligns[number];
    justify?: typeof RowJustify[number];
    prefixCls?: string;
    wrap?: boolean;
    containerClassName?: string;
    containerStyle?: CSSProperties;
    containerAttributes?: Record<string, string | boolean>;
    overflow?: boolean;
}

export const Row: FunctionComponent<React.PropsWithChildren<RowProps>> = ({
    prefixCls: customizePrefixCls,
    justify,
    align,
    className,
    style,
    children,
    gutter = 0,
    wrap,
    containerClassName,
    containerStyle,
    overflow,
    containerAttributes,
    ...others
}) => {
    const [screens, setScreens] = useState<ScreenMap>({
        xs: true,
        sm: true,
        md: true,
        lg: true,
        xl: true,
        xxl: true,
        xxxl: true,
    });

    useEffect(() => {
        const token = ResponsiveObserve.subscribe((screen) => {
            const currentGutter = gutter || 0;
            if (
                (!Array.isArray(currentGutter) && typeof currentGutter === "object") ||
                (Array.isArray(currentGutter) &&
                    (typeof currentGutter[0] === "object" || typeof currentGutter[1] === "object"))
            ) {
                setScreens(screen);
            }
        });
        return () => ResponsiveObserve.unsubscribe(token);
    }, [gutter]);

    const getGutter = (): [number, number] => {
        const results: [number, number] = [0, 0];
        const normalizedGutter = Array.isArray(gutter) ? gutter : [gutter, 0];
        normalizedGutter.forEach((g, index) => {
            if (typeof g === "object") {
                for (let i = 0; i < responsiveArray.length; i++) {
                    const breakpoint: Breakpoint = responsiveArray[i];
                    if (screens[breakpoint] && g[breakpoint] !== undefined) {
                        results[index] = g[breakpoint] as number;
                        break;
                    }
                }
            } else {
                results[index] = g || 0;
            }
        });
        return results;
    };

    const prefixCls = "row";
    const gutters = getGutter();
    const classes = cx(
        prefixCls,
        {
            [classify(prefixCls, "noWrap")]: wrap === false,
            [classify(prefixCls, "full")]: overflow,
            [classify(prefixCls, justify)]: justify,
            [classify(prefixCls, align)]: align,
        },
        className,
        // containerClassName is unnecessary but we still merge is for convenience
        {
            [containerClassName]: containerClassName && overflow,
        },
    );

    const containerClasses = cx("container", containerClassName);

    const rowStyle = {
        ...(gutters[0]! > 0
            ? {
                  marginLeft: gutters[0]! / -2,
                  marginRight: gutters[0]! / -2,
              }
            : {}),
        ...(gutters[1]! > 0
            ? {
                  marginTop: gutters[1]! / -2,
                  marginBottom: gutters[1]! / -2,
              }
            : {}),
        // Calculate width + gutters
        ...(!overflow && gutter[0]! > 0
            ? {
                  width: maxWidth + gutter[0]!,
              }
            : {}),
        ...style,
    };

    return (
        <RowContext.Provider value={{ gutter: gutters, wrap }}>
            {!overflow ? (
                <section
                    className={containerClasses}
                    style={containerStyle}
                    {...containerAttributes}
                >
                    <div {...others} className={classes} style={rowStyle}>
                        {children}
                    </div>
                </section>
            ) : (
                <section {...others} className={classes} style={rowStyle}>
                    {children}
                </section>
            )}

            <style jsx>{`
                div {
                    width: calc(${themeSettings.gridWidth} + ${gutters[0] || 0}px);
                }
            `}</style>
        </RowContext.Provider>
    );
};
