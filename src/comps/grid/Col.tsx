import React, { CSSProperties, FunctionComponent, useContext } from "react";
import RowContext from "@components/grid/RowContext";
import { FlexType, parseFlex } from "@utils/parseFlex";
import { classify } from "@utils/classify";
import styles from "@styles/grid/grid.module.scss";
import classNames from "classnames/bind";

const cx = classNames.bind(styles);

type ColSpanType = number | string;

export interface ColSize {
    flex?: FlexType;
    span?: ColSpanType;
    order?: ColSpanType;
    offset?: ColSpanType;
    push?: ColSpanType;
    pull?: ColSpanType;
}

export interface ColProps extends React.HTMLAttributes<HTMLDivElement> {
    flex?: FlexType;
    span?: ColSpanType;
    order?: ColSpanType;
    offset?: ColSpanType;
    push?: ColSpanType;
    pull?: ColSpanType;
    xs?: ColSpanType | ColSize;
    sm?: ColSpanType | ColSize;
    md?: ColSpanType | ColSize;
    lg?: ColSpanType | ColSize;
    xl?: ColSpanType | ColSize;
    xxl?: ColSpanType | ColSize;
    xxxl?: ColSpanType | ColSize;
    prefixCls?: string;
    content?: {
        align?: {
            horizontal?: string;
            vertical?: string;
        };
    };
}

const sizes = ["xs", "sm", "md", "lg", "xl", "xxl", "xxxl"] as const;

export const Col: FunctionComponent<React.PropsWithChildren<ColProps>> = ({
    prefixCls: customizePrefixCls,
    span,
    order,
    offset,
    push,
    pull,
    className,
    children,
    flex,
    style,
    content = {},
    ...others
}) => {
    const { gutter, wrap } = useContext(RowContext);

    const prefixCls = "col";

    let sizeClassObj = {};
    sizes.forEach((size) => {
        let sizeProps: ColSize = {};

        const propSize = others[size];
        if (typeof propSize === "number") {
            sizeProps.span = propSize;
        } else if (typeof propSize === "object") {
            sizeProps = propSize || {};
        }

        delete others[size];

        sizeClassObj = {
            ...sizeClassObj,
            [classify(prefixCls, size, sizeProps.span)]: sizeProps.span !== undefined,
            [classify(prefixCls, size, "order", sizeProps.order)]:
                sizeProps.order || sizeProps.order === 0,
            [classify(prefixCls, size, "offset", sizeProps.offset)]:
                sizeProps.offset || sizeProps.offset === 0,
            [classify(prefixCls, size, "push", sizeProps.push)]:
                sizeProps.push || sizeProps.push === 0,
            [classify(prefixCls, size, "pull", sizeProps.pull)]:
                sizeProps.pull || sizeProps.pull === 0,
        };
    });

    const classes = cx(
        prefixCls,
        {
            [classify(prefixCls, span)]: span !== undefined,
            [classify(prefixCls, "order", order)]: order,
            [classify(prefixCls, "offset", offset)]: offset,
            [classify(prefixCls, "push", push)]: push,
            [classify(prefixCls, "pull", pull)]: pull,
        },
        className,
        sizeClassObj,
    );

    let mergedStyle: CSSProperties = { ...style };

    if (gutter) {
        mergedStyle = {
            ...(gutter[0]! > 0
                ? {
                      paddingLeft: gutter[0]! / 2,
                      paddingRight: gutter[0]! / 2,
                  }
                : {}),
            ...(gutter[1]! > 0
                ? {
                      paddingTop: gutter[1]! / 2,
                      paddingBottom: gutter[1]! / 2,
                  }
                : {}),
            ...mergedStyle,
        };
    }

    if (content?.align) {
        mergedStyle = {
            display: "flex",
            flexDirection: "column",
            ...(content.align.horizontal ? { alignItems: content.align.horizontal } : {}),
            ...(content.align.vertical ? { justifyContent: content.align.vertical } : {}),
            ...mergedStyle,
        };
    }

    if (flex) {
        mergedStyle.flex = parseFlex(flex);

        // Hack for Firefox to avoid size issue
        // https://github.com/ant-design/ant-design/pull/20023#issuecomment-564389553
        if (flex === "auto" && wrap === false && !mergedStyle.minWidth) {
            mergedStyle.minWidth = 0;
        }
    }

    return (
        <div {...others} style={mergedStyle} className={classes}>
            {children}
        </div>
    );
};
