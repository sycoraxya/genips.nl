import { SecondaryButton } from "@components/button/SecondaryButton";
import React, { FunctionComponent, MouseEvent } from "react";
import { UrlObject } from "url";

export interface ButtonProps {
    type: ButtonType;
    label: string;
    onClick?: (event: MouseEvent<HTMLAnchorElement>) => void;
    href?: string | UrlObject;
    className?: string;
    newTab?: boolean;
    startIcon?: any;
    endIcon?: React.ReactElement;
    iconPosition?: "start" | "end";
}

export enum ButtonType {
    Primary,
    Secondary,
}

export const Button: FunctionComponent<React.PropsWithChildren<ButtonProps>> = ({ type, ...restProps }) => {
    return type === ButtonType.Primary ? null : <SecondaryButton {...restProps} />;
};
