import React, { FunctionComponent } from "react";
import { ButtonProps } from "@components/button";
import Image from "next/image";
import styles from "@styles/button/button.module.scss";
import classNames from "classnames/bind";
import { imageLoader } from "@utils/imageLoader";
import Link from "next/link";
import arrowSvg from "../../../public/long-arrow-forward.svg";

const cx = classNames.bind(styles);

export const SecondaryButton: FunctionComponent<React.PropsWithChildren<Omit<ButtonProps, "type">>> = ({
    label,
    href,
    onClick,
    className,
    newTab,
    startIcon,
    endIcon,
    iconPosition = "end",
}) => {
    const classNames = cx(styles.secondary, className);
    const targetBlank = newTab ? { target: "_blank", rel: "noopener noreferrer" } : {};

    if (href) {
        return (
            (<Link href={href} className={classNames} onClick={onClick} {...targetBlank}>

                {iconPosition === "start" && (
                    <span className={styles.arrowStart}>
                        <svg
                            width="16"
                            height="8"
                            viewBox="0 0 16 8"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M12.01 3H0V5H12.01V8L16 4L12.01 0V3Z"
                                fill="currentColor"
                            />
                        </svg>
                    </span>
                )}
                {label}
                {iconPosition === "end" && (
                    <span className={styles.arrow}>
                        <svg
                            width="16"
                            height="8"
                            viewBox="0 0 16 8"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M12.01 3H0V5H12.01V8L16 4L12.01 0V3Z"
                                fill="currentColor"
                            />
                        </svg>
                    </span>
                )}

            </Link>)
        );
    }

    return (
        <span className={classNames} onClick={onClick}>
            {iconPosition === "start" && (
                <span className={styles.arrowStart}>
                    <svg
                        width="16"
                        height="8"
                        viewBox="0 0 16 8"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path d="M12.01 3H0V5H12.01V8L16 4L12.01 0V3Z" fill="currentColor" />
                    </svg>
                </span>
            )}
            {label}
            {iconPosition === "end" && (
                <span className={styles.arrow}>
                    <svg
                        width="16"
                        height="8"
                        viewBox="0 0 16 8"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path d="M12.01 3H0V5H12.01V8L16 4L12.01 0V3Z" fill="currentColor" />
                    </svg>
                </span>
            )}
        </span>
    );
};
