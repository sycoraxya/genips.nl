import React, { forwardRef, FunctionComponent, HtmlHTMLAttributes } from "react";
import styles from "@styles/typography/typography.module.scss";
import classNames from "classnames/bind";
import { TextTagProps } from "@interfaces/text-tag-props.interface";
import { useTextTags } from "@hooks/useTextTags";
import { classify } from "@utils/classify";

const cx = classNames.bind(styles);

export interface TitleProps extends HtmlHTMLAttributes<"h1">, TextTagProps {
    level?: number;
    visualLevel?: number;
    className?: string;
    div?: boolean;
}

export const Title = forwardRef<HTMLDivElement, TitleProps>(
    (
        { level = "1", visualLevel, className, del, mark, italic, underline, code, children, div },
        ref,
    ) => {
        const Tag = div ? `div` : (`h${level}` as keyof JSX.IntrinsicElements);
        const classes = cx("title", classify("title", `level${visualLevel || level}`), className);
        const content = useTextTags({ del, mark, italic, underline, code }, children);

        return (
            // @ts-ignore
            <Tag className={classes} ref={ref}>
                {content}
            </Tag>
        );
    },
);

Title.displayName = "Title";
