import React, { FunctionComponent } from "react";
import styles from "@styles/typography/typography.module.scss";
import classNames from "classnames/bind";
import { TextTagProps } from "@interfaces/text-tag-props.interface";
import { useTextTags } from "@hooks/useTextTags";

const cx = classNames.bind(styles);

export interface ParagraphProps extends TextTagProps {}

export const Text: FunctionComponent<React.PropsWithChildren<ParagraphProps>> = ({
    del,
    mark,
    italic,
    underline,
    code,
    children,
}) => {
    const classes = cx("text");
    const content = useTextTags({ del, mark, italic, underline, code }, children);

    return <span className={classes}>{content}</span>;
};
