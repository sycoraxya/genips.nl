import React, { forwardRef, HtmlHTMLAttributes } from "react";
import styles from "@styles/typography/typography.module.scss";
import classNames from "classnames/bind";
import { classify } from "@utils/classify";
import { TextTagProps } from "@interfaces/text-tag-props.interface";
import { useTextTags } from "@hooks/useTextTags";

const cx = classNames.bind(styles);

export interface ParagraphProps extends HtmlHTMLAttributes<"div">, TextTagProps {
    size?: "big" | "small" | "medium";
    className?: string;
}

export const Paragraph = forwardRef<HTMLDivElement, ParagraphProps>(
    ({ size = "big", className, del, mark, italic, underline, code, children }, ref) => {
        const classes = cx("paragraph", classify("paragraph", size), className);
        const content = useTextTags({ del, mark, italic, underline, code }, children);

        return (
            <div className={classes} ref={ref}>
                {content}
            </div>
        );
    },
);

Paragraph.displayName = "Paragraph";

// TODO: Add Text element https://ant.design/components/typography/
