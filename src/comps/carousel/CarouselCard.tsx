import React, { FunctionComponent } from "react";
import styles from "@styles/carousel/carouselCard.module.scss";
import Image from "next/image";

interface CaruoselCardProps {
    companyName: string;
    name: string;
    review: string;
}

export const CarouselCard: FunctionComponent<React.PropsWithChildren<CaruoselCardProps>> = ({
    review,
    name,
    companyName,
}) => {
    return (
        <div className={styles.root}>
            <div className={styles.review}>{review}</div>
            <div className={styles.person}>
                <div className={styles.name}>{name}</div>
                <div className={styles.company}>{companyName}</div>
            </div>
        </div>
    );
};
