import React, { FunctionComponent, PropsWithChildren } from "react";
import MultiCarousel from "react-multi-carousel";

export const Carousel: FunctionComponent<React.PropsWithChildren<PropsWithChildren>> = ({ children }) => {
    const responsive = {
        desktop: {
            breakpoint: { max: 4000, min: 1680 },
            items: 4,
            partialVisibilityGutter: 30,
        },
        tablet: {
            breakpoint: { max: 1680, min: 768 },
            items: 2,
            partialVisibilityGutter: 30,
        },
        mobile: {
            breakpoint: { max: 768, min: 0 },
            items: 1,
            partialVisibilityGutter: 30,
        },
    };

    return (
        <MultiCarousel
            responsive={responsive}
            arrows={false}
            partialVisible={true}
            transitionDuration={1}
            ssr={true}
        >
            {children}
        </MultiCarousel>
    );
};
