import { FunctionComponent, PropsWithChildren, useContext, useEffect } from "react";
import { initGa, logPageView } from "../utils/analytics";
import { HeaderContext } from "../components/header/context";
import { usePageTransitionTimeout } from "../hooks/usePageTransitionTimeout";

interface LayoutProps {
    overlayColor?: "white";
    overlay?: boolean;
}

export const Layout: FunctionComponent<React.PropsWithChildren<PropsWithChildren<LayoutProps>>> = ({
    children,
    overlayColor,
    overlay,
}) => {
    useEffect(() => {
        // @ts-ignore
        if (!window.GA_INITIALIZED) {
            initGa();
            // @ts-ignore
            window.GA_INITIALIZED = true;
        }

        logPageView();
    }, []);
    const { menuIsOpen, toggleMenu } = useContext(HeaderContext);

    usePageTransitionTimeout(() => window.scrollTo(0, 0));

    // Close menu while transitioning page
    usePageTransitionTimeout(() => (menuIsOpen ? toggleMenu() : null), [menuIsOpen]);

    return <main>{children}</main>;
};
