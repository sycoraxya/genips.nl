import React, { FunctionComponent, PropsWithChildren } from "react";
import { Title } from "@components/typography";
import { Row, Col } from "@components/grid";
import { ImageProp } from "@interfaces/image-prop.interface";
import { UrlObject } from "url";
import styles from "@styles/people/aboutMe.module.scss";
import typographyStyles from "@styles/typography/typography.module.scss";
import classNames from "classnames/bind";
import Image from "next/image";
import { Button, ButtonType } from "@components/button";
import gridHelperStyles from "@styles/grid/helpers.module.scss";

const cx = classNames.bind({ ...styles, ...typographyStyles });

interface AboutMeProps {
    name: string;
    jobTitle: string;
    image: ImageProp;
    additionalInformation?: {
        label?: string;
        href: string | UrlObject;
    };
    phone?: {
        label?: string;
        value: string;
    };
    email?: {
        label?: string;
        value: string;
    };
    imageOrder?: 0 | 1;
}

export const AboutMe: FunctionComponent<React.PropsWithChildren<PropsWithChildren<AboutMeProps>>> = ({
    name,
    jobTitle,
    image,
    additionalInformation,
    children,
    phone,
    email,
    imageOrder = 0,
}) => {
    const jobTitleClasses = cx("faded", "jobTitle");

    return (
        <Row
            containerClassName={gridHelperStyles.ySpacing}
            gutter={[{ lg: 70 }, { lg: 0, xs: 14, xxs: 14 }]}
        >
            <Col span={12} lg={{ span: 6, order: imageOrder }} order={0}>
                <div className={styles.imageWrapper}>
                    <Image
                        src={`/` + image.src}
                        alt={image.alt}
                        layout="fill"
                        objectFit={image.objectFit ?? "cover"}
                        objectPosition={(image.objectPosition as string) ?? "center"}
                    />
                </div>
            </Col>
            <Col span={12} lg={6}>
                <Title visualLevel={5} div className={jobTitleClasses}>
                    {jobTitle} —
                </Title>
                <Title visualLevel={2} div className={styles.name}>
                    {name}
                </Title>
                {children}
                {additionalInformation && (
                    <Button
                        type={ButtonType.Secondary}
                        label={additionalInformation.label ?? `Lees meer over ${name}`}
                        href={additionalInformation.href}
                        className={styles.additionalInformation}
                        newTab={true}
                    />
                )}
                {email && (
                    <Button
                        type={ButtonType.Secondary}
                        label={email.label ?? `Mail ${name}`}
                        href={email.value}
                        className={styles.additionalInformation}
                    />
                )}
                {phone && (
                    <Button
                        type={ButtonType.Secondary}
                        label={phone.label ?? `Bel ${name}`}
                        href={phone.value}
                        className={styles.additionalInformation}
                    />
                )}
            </Col>
        </Row>
    );
};
