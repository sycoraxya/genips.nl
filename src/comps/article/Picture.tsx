import React, { FunctionComponent } from "react";
import styles from "@styles/insights/article.module.scss";
import Image from "next/image";

interface Source {
    srcSet: string;
    media: string;
}

interface Img {
    src: string;
    alt: string;
    width: number;
    height: number;
}

interface PictureProps {
    sources: Source[];
    img: Img;
}

export const Picture: FunctionComponent<React.PropsWithChildren<PictureProps>> = ({ sources, img }) => {
    return (
        <div className={styles.imageWrapper}>
            {/* <picture>
                {sources &&
                    sources.map((source, index) => {
                        return (
                            <source
                                key={`${source.srcSet}_${index}`}
                                media={source.media}
                                srcSet={source.srcSet}
                            />
                        );
                    })}
                <img
                    src={img.src}
                    alt={img.alt}
                    loading="lazy"
                    width={img.width}
                    height={img.height}
                />
            </picture> */}
            <Image src={img.src} alt={img.alt} width={img.width} height={img.height} />
        </div>
    );
};
