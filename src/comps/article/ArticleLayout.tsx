import React, { FunctionComponent, PropsWithChildren } from "react";
import { useAppContext } from "@hooks/useAppContext";
import { ArticleMeta, Paragraph, Section, Title, Picture } from "@components/article";
import { Col } from "@components/col";
import { Divider } from "@components/divider";
import { Row } from "@components/row";
import { Layout } from "@components/Layout";
import { HeadMeta } from "src/components/HeadMeta";
import Head from "next/head";
import { Tag } from "@components/tag";
import { getFormattedDate, getHeroImage } from "@components/article/util";
import { MDXProvider } from "@mdx-js/react";
import { ArticleJsonLd } from "next-seo";
import Image from "next/image";

import styles from "@styles/insights/article.module.scss";

interface ArticleLayoutProps extends ArticleMeta {}

const components = {
    h1: Title,
    h2: Title,
    p: Paragraph,
    Section,
    Picture: (props: any) => <Picture {...props} />,
};

export const ArticleLayout: FunctionComponent<React.PropsWithChildren<PropsWithChildren<ArticleLayoutProps>>> = ({
    title,
    description,
    date,
    tags,
    staticImage,
    children,
    slug,
}) => {
    useAppContext({ heroColor: "white" });
    const imagePath = staticImage.src.replace(/^\/+/, "");
    const host = process.env.env === "staging" ? `https://staging.genips.nl` : `https://genips.nl`;
    const fullImage = `${host}/${imagePath}`;

    return (
        <Layout>
            <HeadMeta
                title={title}
                description={description}
                image={staticImage.src}
                canonicalSlug={`/insights/${slug}`}
                type="article"
                article={{
                    publishedTime: date.toISOString(),
                    modifiedTime: date.toISOString(),
                }}
            />
            <ArticleJsonLd
                title={title}
                url={`${host}/insights/${slug}`}
                images={[fullImage]}
                datePublished={date.toISOString()}
                authorName={"Genips"}
                description={description}
                publisherName={"Genips"}
                publisherLogo={`${host}/genips-logo-jpg.jpg`}
            />
            <Head>
                <meta property="article:published_time" content={date.toISOString()} />
            </Head>
            <Row justify={"center"} className={styles.head}>
                <Col span={12} md={8}>
                    <h1 className={styles.heading}>{title}</h1>
                </Col>
            </Row>
            <Row overflow>
                <Col span={12}>
                    <div className={styles.heroImageWrapper}>
                        <Image
                            src={staticImage}
                            alt={title}
                            layout="fill"
                            placeholder={"blur"}
                            objectFit="cover"
                        />
                    </div>
                </Col>
            </Row>
            <MDXProvider components={components}>
                <article>{children}</article>
            </MDXProvider>
            <Section>
                <Divider style={{ margin: 0 }} />
            </Section>
        </Layout>
    );
};
