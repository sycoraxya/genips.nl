import React, { FunctionComponent, PropsWithChildren } from "react";
import styles from "@styles/insights/article.module.scss";

export const Title: FunctionComponent<React.PropsWithChildren<PropsWithChildren>> = ({ children }) => {
    return <h2 className={styles.title}>{children}</h2>;
};
