export { Section } from "./Section";

export { Title } from "./Title";

export { Paragraph } from "./Paragraph";

export { ArticleLayout } from "./ArticleLayout";

export { Picture } from "./Picture";

export { ArticleTags, getHeroImage, getFormattedDate } from "./util";
import { StaticImageData } from "next/image";
import type { ArticleTag } from "./util";
export type { ArticleTag } from "./util";

export interface ArticleMeta {
    title: string;
    date: Date;
    tags: ArticleTag[];
    staticImage?: StaticImageData;
    description: string;
    slug: string;
}
