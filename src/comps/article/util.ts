export type ArticleTag = { text: string; color: string };

export const ArticleTags = {
    design: {
        text: "design",
        color: "genipsBlue",
    },
    development: {
        text: "development",
        color: "genipsBlue",
    },
    copywriting: {
        text: "copywriting",
        color: "genipsBlue",
    },
    website: {
        text: "website",
        color: "genipsRed",
    },
    webshop: {
        text: "webshop",
        color: "genipsRed",
    },
    dashboard: {
        text: "dashboard",
        color: "genipsRed",
    },
    landingspagina: {
        text: "landingspagina",
        color: "genipsRed",
    },
    onderhoud: {
        text: "onderhoud",
        color: "genipsDarkBlue",
    },
    conversieOptimalisatie: {
        text: "conversie optimalisatie",
        color: "genipsDarkBlue",
    },
    seo: {
        text: "seo",
        color: "genipsDarkBlue",
    },
    sea: {
        text: "sea",
        color: "genipsDarkBlue",
    },
};

export const getHeroImage = (
    heroImage: { path: string; filename: string },
    size: "full" | "medium" | "small" | "thumb",
) => {
    return `${heroImage.path}${heroImage.filename}_${size}.jpg`;
};

export const getFormattedDate = (date: Date) => {
    return new Intl.DateTimeFormat("nl-NL", { month: "long", year: "numeric" }).format(date);
};
