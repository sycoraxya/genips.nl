import React, { FunctionComponent, PropsWithChildren } from "react";
import styles from "@styles/insights/article.module.scss";

export const Paragraph: FunctionComponent<React.PropsWithChildren<PropsWithChildren>> = ({ children }) => {
    return <p className={styles.paragraph}>{children}</p>;
};
