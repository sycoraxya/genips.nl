import React, { FunctionComponent, PropsWithChildren } from "react";
import styles from "@styles/insights/article.module.scss";
import { Col } from "@components/col";
import { Row } from "@components/row";

export const Section: FunctionComponent<React.PropsWithChildren<PropsWithChildren>> = ({ children }) => {
    return (
        <Row justify={"center"} className={styles.section}>
            <Col span={12} md={8} xl={6}>
                {children}
            </Col>
        </Row>
    );
};
