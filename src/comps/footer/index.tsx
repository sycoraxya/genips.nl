import { Col } from "@components/col";
import { Row } from "@components/row";
import React, { FunctionComponent } from "react";
import Link from "next/link";

import styles from "@styles/footer/footer.module.scss";

export const Footer: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const date = new Date();

    return (
        <footer className={styles.root}>
            <Row className={styles.row} justify={"center"} gutter={[0, 40]}>
                <Col span={12} lg={3}>
                    <img
                        src="/genips-logo-black.svg"
                        alt="Genips Logo"
                        className={styles.logo}
                        width="160"
                        height="80"
                    />
                </Col>
                <Col span={12} lg={3}>
                    <span className={styles.heading}>Contact —</span>
                    <ul>
                        <li>
                            <a
                                href="mailto:hallo@genips.nl"
                                onClick={() =>
                                    //@ts-ignore
                                    window.gtag("event", "Click", {
                                        category: "Contact",
                                        label: "Clicked email address footer",
                                    })
                                }
                            >
                                hallo@genips.nl
                            </a>
                        </li>
                        <li>
                            <a
                                href="tel:+31 6 254 630 10"
                                onClick={() =>
                                    //@ts-ignore
                                    window.gtag("event", "Click", {
                                        category: "Contact",
                                        label: "Clicked phone number footer",
                                    })
                                }
                            >
                                +31 6 254 630 10
                            </a>
                        </li>
                    </ul>
                </Col>
                <Col span={12} lg={3}>
                    <span className={styles.heading}>Social —</span>
                    <ul>
                        <li>
                            <a
                                href="https://www.linkedin.com/company/genips"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Linkedin
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.facebook.com/genipsagency"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Facebook
                            </a>
                        </li>
                    </ul>
                </Col>
                <Col span={12} lg={3}>
                    <span className={styles.heading}>Genips B.V. —</span>
                    <ul>
                        <li>Meidoornhof 19</li>
                        <li>3831 XR Leusden</li>
                        <li>KVK 92267114</li>
                        <li>BTW-ID NL865965766B01</li>
                    </ul>
                </Col>
            </Row>

            <Row
                className={`${styles.row}`}
                containerClassName={styles.bottomBar}
                justify={"spaceBetween"}
                gutter={[0, 40]}
            >
                <Col span={12} order={1} xxxl={{ span: 4, order: 0 }}>
                    <span className={styles.copyright}>
                        © {date.getFullYear()} Genips B.V. — Alle rechten voorbehouden.
                    </span>
                </Col>
                <Col span={12} order={0} xxxl={{ span: 8, order: 1 }} className={styles.terms}>
                    <Link href="/algemene-voorwaarden" target="_blank">
                        Algemene Voorwaarden
                    </Link>
                    <span className={styles.divider} />
                    <Link href="/algemene-voorwaarden-hosting" target="_blank">
                        Algemene Voorwaarden Hosting
                    </Link>
                    <span className={styles.divider} />
                    <Link href="/privacybeleid">Privacybeleid</Link>
                    <span className={styles.divider} />
                    <a href="mailto:support@genips.nl">Support</a>
                </Col>
            </Row>
        </footer>
    );
};
