import { Paragraph, Title } from "@components/typography";
import styles from "@styles/home/steps.module.scss";
import { FunctionComponent, PropsWithChildren } from "react";
import typographyStyles from "@styles/typography/typography.module.scss";

interface StepProps {
    title: string;
}

export const Step: FunctionComponent<React.PropsWithChildren<PropsWithChildren<StepProps>>> = ({ title, children }) => {
    return (
        <span className={styles.step}>
            <div className={styles.timeline} />
            <Title visualLevel={4} div className={styles.stepTitle}>
                {title}
            </Title>
            <Paragraph size="medium" className={typographyStyles.faded}>
                {children}
            </Paragraph>
        </span>
    );
};
