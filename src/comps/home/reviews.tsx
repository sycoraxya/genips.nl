import { Carousel } from "@components/carousel";
import { CarouselCard } from "@components/carousel/CarouselCard";
import { Row, Col } from "@components/grid";
import React, { FunctionComponent } from "react";
import styles from "@styles/home/steps.module.scss";
import { Paragraph, Title } from "@components/typography";
import typographyStyles from "@styles/typography/typography.module.scss";
import classNames from "classnames";

export const Reviews: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const leadClassName = classNames(typographyStyles.faded, styles.subtitle);

    return (
        <Row containerClassName={styles.root}>
            <Col span={12}>
                <div className={styles.titleContainer}>
                    <Title level={3} visualLevel={5} className={leadClassName}>
                        Beoordelingen —
                    </Title>
                    <Title level={4} visualLevel={1} className={styles.title}>
                        Wat vinden <br />
                        <b>onze klanten?</b>
                    </Title>
                </div>
                <Carousel>
                    <CarouselCard
                        companyName="Tricorp International"
                        name="Max van Domburg"
                        review="“Naast dat Genips kwaliteit levert is de samenwerking ook erg aangenaam. Een fijn en vooral laagdrempelig contact wat prettig werkt. Genips komt proactief met ideeën om de webshop te optimaliseren. Transparant als het gaat om kosten en gemaakte uren en denkt hierbij ook in het belang van de opdrachtgever. Genips is flexibel en werkt op een vanzelfsprekende manier samen met andere leveranciers die van belang zijn voor de webshop. Top!”"
                    />
                    <CarouselCard
                        companyName="Bijzondere Camping"
                        name="Lotte Jongepier"
                        review="“Erg tevreden over het onderhoudspakket! Ze zijn altijd goed bereikbaar, super snel en betaalbaar. Zat al 2 jaar met een probleem waar geen oplossing voor leek te zijn; Genips heeft het binnen 2 uurtjes opgelost. Heel blij mee :-)”"
                    />
                    <CarouselCard
                        companyName="Step Events"
                        name="Ed Weyand"
                        review="“Genips werkt snel en efficiënt tegen een zeer redelijke vergoeding. Creativiteit en kennis staan op een hoog niveau. Hun inzet heeft Step Events een nieuwe omzetimpuls gegeven.”"
                    />
                    <CarouselCard
                        companyName="Multimedia Group"
                        name="Merlijn Witteveen"
                        review="“Via freelancer.nl ben ik bij deze 2 web professionals terecht gekomen. Ze leveren een super service en ze denken goed met je mee! Zonder twijfel alles bij ze ondergebracht van hosting tot services een aanrader als je opzoek bent naar een partner in je online activiteiten.”"
                    />
                    <CarouselCard
                        companyName="Crossbizz"
                        name="Chris Bos"
                        review="“Fijn bedrijf om mee samen te werken. Goed persoonlijk overleg met echte aandacht, heldere analyse, flexibel, snelle concrete acties en resultaat. Ik ben heel tevreden met mijn website. En een hele goede prijs-kwaliteit verhouding. Toppers!”"
                    />
                    <CarouselCard
                        companyName="#LJKuipers"
                        name="Laura Kuipers"
                        review="“Super fijne samenwerking met de mannen van Genips. Snelle en vlotte communicatie. Weten waar ze het over hebben en staan altijd klaar om me te helpen waar nodig.”"
                    />
                    <CarouselCard
                        companyName="Stichting Booster Festival"
                        name="Pim Kokkeler"
                        review="“Heel fijn samengewerkt voor een geavanceerd plansysteem voor het Booster festival. Goede ondersteuning, timeline en oplevering.”"
                    />
                    <CarouselCard
                        companyName="Bijzonder Plekje"
                        name="Marleen Brekelmans"
                        review="“Snel, deskundig en behulpzaam; heel blij dat we het onderhoud en de vernieuwingen aan onze website nu uitbesteden aan Genips. Alleen maar lof voor de snelle service.”"
                    />
                    <CarouselCard
                        companyName="AddVueConnect"
                        name="Vincent Brouwer"
                        review="“Micha en Stefan hebben voor ons een prachtige website gebouwd. Vakkundig en gedreven met goede feedback op mijn eigenwijze ik :-) Genips is een aanrader!”"
                    />
                </Carousel>
            </Col>
        </Row>
    );
};
