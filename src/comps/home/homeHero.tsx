import { FunctionComponent, HTMLAttributes, MutableRefObject, useEffect, useRef } from "react";
import classNames from "classnames";
import styles from "@styles/hero/hero.module.scss";
import homeHeroStyles from "@styles/home/hero.module.scss";
import { Col, Row } from "@components/grid";
import { useAppContext } from "@hooks/useAppContext";
import typographyStyles from "@styles/typography/typography.module.scss";
import { Paragraph, Title } from "@components/typography";
import { Button, ButtonType } from "@components/button";
import Image from "next/image";
import { imageLoader } from "@utils/imageLoader";
import anime from "animejs";
import { AnimatedLink } from "src/components/animatedLink";

interface HomeHeroProps extends HTMLAttributes<HTMLDivElement> {
    color?: "green" | "light" | "dark";
}

export const HomeHero: FunctionComponent<React.PropsWithChildren<HomeHeroProps>> = ({ color }) => {
    const { contextHeroColor } = useAppContext();
    const containerClasses = classNames(styles.root, {
        [styles[color]]: color !== contextHeroColor,
        [styles[contextHeroColor]]: contextHeroColor === color,
    });
    const subtitleClasses = classNames(typographyStyles.faded, homeHeroStyles.lead);

    const introLead = useRef<HTMLHeadingElement>(null);
    const introServices = useRef<HTMLDivElement>(null);
    const introParagraph = useRef<HTMLParagraphElement>(null);
    const imageRef = useRef<HTMLImageElement>(null);

    /* useEffect(() => {
        const timeline = anime.timeline({
            delay: 400,
            easing: "easeInOutQuad",
        });

        timeline.add({
            targets: introLead.current,
            duration: 300,
            paddingTop: ["1.29em", 0],
        });

        timeline.add(
            {
                targets: [...Array.from(introServices.current.children)],
                paddingTop: ["1.7em", 0],
                duration: 300,
            },
            0,
        );

        timeline.add(
            {
                targets: introParagraph.current,
                opacity: [0, 1],
                duration: 300,
            },
            300,
        );

        timeline.add(
            {
                targets: imageRef.current,
                opacity: [0, 1],
                duration: 800,
                easing: "easeOutCubic",
            },
            0,
        );
    }, []); */

    return (
        <Row
            containerClassName={containerClasses}
            gutter={80}
            containerAttributes={{ "data-color": contextHeroColor }}
        >
            <Col
                span={12}
                order={1}
                content={{ align: { vertical: "center" } }}
                lg={{ span: 8, order: 0 }}
                xl={7}
                xxl={6}
                className={homeHeroStyles.leftCol}
            >
                <Title level={1} visualLevel={3} className={subtitleClasses} ref={introLead}>
                    Jouw digitale transformatie begint hier.
                </Title>
                <div className={homeHeroStyles.services} ref={introServices}>
                    <h2 className={homeHeroStyles.service}>
                        <AnimatedLink href="/services/webapplicaties" title="Webapplicatie" />
                    </h2>
                    <h2 className={homeHeroStyles.service}>
                        <AnimatedLink href="/services/webshops" title="E-commerce" />
                    </h2>
                    <h2 className={homeHeroStyles.service}>
                        <AnimatedLink href="/services/shared-hosting" title="Hosting" />
                    </h2>
                </div>
                <span ref={introParagraph} className={homeHeroStyles.introParagraph}>
                    <Paragraph size={"medium"} className={styles.body}>
                        Bij Genips bouwen we niet alleen digitale oplossingen—we creëren ervaringen,
                        bevorderen innovatie en dragen bij aan een duurzame toekomst.
                    </Paragraph>
                    <Button
                        type={ButtonType.Secondary}
                        label="Klaar voor jouw digitale transformatie?"
                        href={"/contact"}
                        className={styles.button}
                    />
                </span>
            </Col>
            <Col span={12} lg={4} xl={5} xxl={6}>
                <span ref={imageRef}>
                    <Image
                        src={"/homepage.svg"}
                        alt={"Homepage"}
                        width={800}
                        height={600}
                        className={styles.image}
                    />
                </span>
            </Col>
        </Row>
    );
};
