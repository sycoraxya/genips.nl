import styles from "@styles/home/steps.module.scss";
import { Step } from "./step";

export const Steps = () => {
    return (
        <div className={styles.stepsContainer}>
            <Step title="Verkennen">
                Samen met jou gaan we op zoek naar de beste manier om je product tot een succes te
                maken.
            </Step>
            <Step title="Ontwerp">
                We vormen een eerste ontwerp op basis van jouw wensen. Voor complexe opdrachten een
                simpele oplossing vinden is onze specialiteit.
            </Step>
            <Step title="Ontwikkeling">
                Genips gebruikt de nieuwste technologie om een efficiënt en flexibel product te
                bouwen dat voor iedereen begrijpelijk is.
            </Step>
            <Step title="Verbeteren">
                Een ontwerp is nooit af. Daarom helpen we je graag ontdekken hoe het in de toekomst
                nóg beter kan.
            </Step>
        </div>
    );
};
