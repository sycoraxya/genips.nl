import { Row, Col } from "@components/grid";
import React, { FunctionComponent } from "react";
import styles from "@styles/home/steps.module.scss";
import { Steps } from "@components/home/steps";
import { Paragraph, Title } from "@components/typography";
import typographyStyles from "@styles/typography/typography.module.scss";
import classNames from "classnames";

export const HomeSteps: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const leadClassName = classNames(typographyStyles.faded, styles.subtitle);

    return (
        <Row containerClassName={styles.root}>
            <Col span={12}>
                <div className={styles.titleContainer}>
                    <Title level={3} visualLevel={5} className={leadClassName}>
                        Onze aanpak —
                    </Title>
                    <Title level={4} visualLevel={1} className={styles.title}>
                        Van complex naar <br />
                        <b>simpel en efficiënt</b>
                    </Title>
                    <Paragraph>
                        Samen creëren we de website of webshop van je dromen in vier overzichtelijke
                        stappen.
                    </Paragraph>
                </div>
                <Steps />
            </Col>
        </Row>
    );
};
