import React, { FunctionComponent } from "react";
import styles from "@styles/treeCounter/treeCounter.module.scss";
import { Title } from "@components/typography";
import { Col, Row } from "@components/grid";

export const TreeCounter: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    return (
        <Row containerClassName={styles.root}>
            <Col span={12} content={{ align: { horizontal: "center" } }}>
                <Title div visualLevel={2} className={styles.title}>
                    Totaal aantal geplante bomen:
                </Title>
                <div className={styles.count}>280</div>
            </Col>
        </Row>
    );
};
