import React, { CSSProperties, FunctionComponent, ReactNode } from "react";
import classNames from "classnames/bind";

import styles from "@styles/divider/divider.module.scss";
import { classify } from "@utils/classify";

const cx = classNames.bind(styles);

export interface DividerProps {
    prefixCls?: string;
    type?: "horizontal" | "vertical";
    orientation?: "left" | "right" | "center";
    className?: string;
    children?: ReactNode;
    dashed?: boolean;
    style?: CSSProperties;
    plain?: boolean;
}

export const Divider: FunctionComponent<React.PropsWithChildren<DividerProps>> = ({
    prefixCls: customizePrefixCls,
    type = "horizontal",
    orientation = "center",
    className,
    children,
    dashed,
    plain,
    ...restProps
}) => {
    const prefixCls = "divider";

    const hasChildren = !!children;
    const classString = cx(
        prefixCls,
        classify(prefixCls, type),
        {
            [classify(prefixCls, "withText")]: hasChildren,
            [classify(prefixCls, "withText", orientation)]: hasChildren,
            [classify(prefixCls, "dashed")]: !!dashed,
            [classify(prefixCls, "plain")]: !!plain,
        },
        className,
    );

    return (
        <div className={classString} {...restProps} role="separator">
            {children && <span className={cx(`${prefixCls}InnerText`)}>{children}</span>}
        </div>
    );
};
