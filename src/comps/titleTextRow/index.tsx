import React, { FunctionComponent, PropsWithChildren } from "react";
import { Title } from "@components/typography";
import { Row, Col } from "@components/grid";
import styles from "@styles/titleTextRow/titleTextRow.module.scss";
import gridHelperStyles from "@styles/grid/helpers.module.scss";
import classNames from "classnames";

interface TitleTextRowProps {
    title: string;
    titleOrder?: 0 | 1;
    textColor?: "light" | "dark";
}

export const TitleTextRow: FunctionComponent<React.PropsWithChildren<PropsWithChildren<TitleTextRowProps>>> = ({
    title,
    titleOrder = 0,
    textColor = "dark",
    children,
}) => {
    const containerClassName = classNames(gridHelperStyles.ySpacing, {
        [styles[textColor]]: textColor !== "dark",
    });
    const titleWrapperClasses = classNames(styles.titleWrapper, {
        [styles.alignRight]: titleOrder === 1,
    });

    return (
        <Row
            containerClassName={containerClassName}
            gutter={[
                { lg: 70, md: 20 },
                { md: 0, xs: 30, xxs: 30 },
            ]}
        >
            <Col
                span={12}
                md={{ span: 6, order: titleOrder }}
                className={titleWrapperClasses}
                order={0}
            >
                <Title visualLevel={2} div>
                    {title}
                </Title>
            </Col>
            <Col span={12} md={6}>
                {children}
            </Col>
        </Row>
    );
};
