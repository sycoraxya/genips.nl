import React, { CSSProperties, FunctionComponent, HTMLAttributes } from "react";
import classNames from "classnames/bind";
import styles from "@styles/tag/tag.module.scss";
import {
    PresetColorType,
    PresetColorTypes,
    PresetStatusColorType,
    PresetStatusColorTypes,
} from "@utils/colors";
import { LiteralUnion } from "@utils/type";
import { classify } from "@utils/classify";

const cx = classNames.bind(styles);

const PresetColorRegex = new RegExp(`^(${PresetColorTypes.join("|")})(-inverse)?$`);
const PresetStatusColorRegex = new RegExp(`^(${PresetStatusColorTypes.join("|")})$`);

export interface TagProps extends HTMLAttributes<HTMLSpanElement> {
    className?: string;
    color?: LiteralUnion<PresetColorType | PresetStatusColorType, string>;
    style?: CSSProperties;
}

export const Tag: FunctionComponent<React.PropsWithChildren<TagProps>> = ({
    children,
    className,
    color,
    style,
    ...props
}) => {
    const isPresetColor = (): boolean => {
        if (!color) {
            return false;
        }
        return PresetColorRegex.test(color) || PresetStatusColorRegex.test(color);
    };

    const tagStyle = {
        backgroundColor: color && !isPresetColor() ? color : undefined,
        ...style,
    };

    const presetColor = isPresetColor();
    const prefixCls = "tag";
    const tagClassName = cx(
        prefixCls,
        {
            [classify(prefixCls, color)]: presetColor,
            [classify(prefixCls, "hasColor")]: color && !presetColor,
        },
        className,
    );

    return (
        <span {...props} className={tagClassName} style={tagStyle}>
            {children}
        </span>
    );
};
