import { ElementOf, tuple } from "@utils/type";

export const PresetStatusColorTypes = tuple("success", "processing", "error", "default", "warning");

export const PresetColorTypes = tuple(
    "pink",
    "red",
    "yellow",
    "orange",
    "cyan",
    "green",
    "blue",
    "purple",
    "geekblue",
    "magenta",
    "volcano",
    "gold",
    "lime",
    "genipsBlue",
    "genipsRed",
    "genipsDarkBlue",
    "genipsOrange",
    "genipsYellow"
);

export type PresetColorType = ElementOf<typeof PresetColorTypes>;
export type PresetStatusColorType = ElementOf<typeof PresetStatusColorTypes>;
