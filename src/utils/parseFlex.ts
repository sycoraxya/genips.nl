export type FlexType = number | "none" | "auto" | string;

export const parseFlex = (flex: FlexType): string => {
    if (typeof flex === "number") {
        return `${flex} ${flex} auto`;
    }

    if (/^\d+(\.\d+)?(px|em|rem|%)$/.test(flex)) {
        return `0 0 ${flex}`;
    }

    return flex;
};
