const capitalize = (s: string) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
};

export const classify = (className: string, ...classNames: (string | number)[]) => {
    return `${className}${classNames
        .map((name) => (typeof name === "string" ? capitalize(name) : name))
        .join("")}`;
};
