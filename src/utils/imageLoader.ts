export const imageLoader = ({
    src,
    width,
    quality,
}: {
    src?: string;
    width?: number;
    quality?: number;
}) => {
    switch (process.env.env) {
        case "development":
            return `http://localhost:3000/${src}`;
            break;
        case "staging":
            return `https://staging.genips.nl/${src}`;
            break;
    }
};
