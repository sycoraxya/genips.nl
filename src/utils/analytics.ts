export const initGa = () => {
    const isLocal = location.hostname === "localhost";
    const isStaging = location.hostname.includes("staging");

    if (isLocal || isStaging) {
        // @ts-ignore
        window.gtag("set", { sendHitTask: null });
    }
};

export const logPageView = () => {
    // @ts-ignore
    window.gtag("config", "G-KPPKJ44CCB", {
        page_path: window.location.pathname,
    });
};
