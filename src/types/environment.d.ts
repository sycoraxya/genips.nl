declare global {
    namespace NodeJS {
        interface ProcessEnv {
            MAIL_PORT: number;
            MAIL_HOST: string;
            MAIL_USER: string;
            MAIL_PASS: string;
            API_SECRET: string;
            RECAPTCHA_SECRET: string;
            NEXT_PUBLIC_RECAPTCHA_SITE_KEY: string;
        }
    }
}

export {};
