import { FunctionComponent, PropsWithChildren } from "react";
import styles from "../../styles/5-components/services/service.module.scss";
import Link from "next/link";

interface ServiceProps {
    title: string;
    megaTitle?: string;
    url?: string;
}

export const Service: FunctionComponent<
    React.PropsWithChildren<PropsWithChildren<ServiceProps>>
> = ({ title, megaTitle, url, children }) => {
    if (url) {
        return (
            <Link href={url} scroll={false} className={styles.root}>
                <span className={styles.megaTitle}>{megaTitle || title}</span>
                <h3 className={styles.title}>{title}</h3>
                <p className={styles.paragraph}>{children}</p>
                <span className={styles.iconWrapper}>
                    <img src="/arrow-forward.svg" className={styles.icon} alt={"Go to Service"} />
                </span>
            </Link>
        );
    }

    return (
        <span className={styles.root}>
            <span className={styles.megaTitle}>{megaTitle || title}</span>
            <h3>{title}</h3>
            <p className={styles.paragraph}>{children}</p>
        </span>
    );
};
