import { FunctionComponent } from "react";
import styles from "../../styles/5-components/services/backToServices.module.scss";
import Link from "next/link";

export const BackToServices: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    return (
        (<Link href="/services" scroll={false} className={styles.root}>

            <img src="/arrow-forward.svg" alt={"Back to Services"} />services
        </Link>)
    );
};
