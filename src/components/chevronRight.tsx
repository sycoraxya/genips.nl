import React, { FunctionComponent } from "react";

interface ChevronRightProps {
    size?: number;
    color?: string;
}

export const ChevronRight: FunctionComponent<React.PropsWithChildren<ChevronRightProps>> = ({
    size = 18,
    color = "#ffffff",
}) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        viewBox="0 0 24 24"
        fill="none"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="square"
    >
        <path d="M9 18l6-6-6-6" />
    </svg>
);
