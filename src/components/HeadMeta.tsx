import { NextSeo } from "next-seo";
import React, { FunctionComponent } from "react";
import Script from "next/script";
import { OpenGraphArticle } from "next-seo/lib/types";

interface HeadMetaProps {
    title: string;
    description: string;
    image?: string;
    type?: "website" | "article";
    canonicalSlug: string;
    article?: OpenGraphArticle;
}

declare global {
    interface Window {
        dataLayer: any;
    }
}

export const HeadMeta: FunctionComponent<React.PropsWithChildren<HeadMetaProps>> = ({
    title,
    description,
    image = "genips-logo-jpg.jpg",
    canonicalSlug,
    type = "website",
    article,
}) => {
    const imagePath = image.replace(/^\/+/, "");
    const fullImage =
        process.env.env === "staging"
            ? `https://staging.genips.nl/${imagePath}`
            : `https://genips.nl/${imagePath}`;
    const canonicalUrl = `${
        process.env.env === "staging" ? "https://staging.genips.nl" : "https://genips.nl"
    }${canonicalSlug}`;

    return (
        <>
            <NextSeo
                titleTemplate={"Genips — %s"}
                title={title}
                canonical={canonicalUrl}
                description={description}
                noindex={process.env.env === "staging"}
                nofollow={process.env.env === "staging"}
                openGraph={{
                    title,
                    description,
                    url: canonicalUrl,
                    type,
                    images: [
                        {
                            url: fullImage,
                            alt: "Genips",
                        },
                    ],
                    article,
                }}
                additionalLinkTags={[
                    {
                        rel: "icon",
                        href: "/favicon.svg",
                    },
                ]}
                twitter={{
                    cardType: "summary_large_image",
                }}
                additionalMetaTags={[
                    {
                        name: "viewport",
                        content: "width=device-width, initial-scale=1.0",
                    },
                ]}
            />

            <Script
                src={"https://www.googletagmanager.com/gtag/js?id=G-KPPKJ44CCB"}
                strategy={"afterInteractive"}
                defer={true}
            />
            <Script id="google-tag-manager" strategy={"afterInteractive"}>
                {process.env.env !== "production"
                    ? `window["ga-disable-G-KPPKJ44CCB"] = true;`
                    : ""}
                {`window.dataLayer = window.dataLayer || [];
        function gtag() {
            window.dataLayer.push(arguments);
        }

        gtag("js", new Date());

        gtag("config", "G-KPPKJ44CCB");`}
            </Script>
        </>
    );
};
