import styles from "../../styles/5-components/home/steps.module.scss";
import { FunctionComponent, PropsWithChildren } from "react";

interface StepProps {
    title: string;
}

export const Step: FunctionComponent<React.PropsWithChildren<PropsWithChildren<StepProps>>> = ({ title, children }) => {
    return (
        <span className={styles.step}>
            <div className={styles.timeline} />
            <h2>{title}</h2>
            <p>{children}</p>
        </span>
    );
};
