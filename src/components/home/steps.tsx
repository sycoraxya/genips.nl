import styles from "../../styles/5-components/home/steps.module.scss";
import { Step } from "./step";

export const Steps = () => {
    return (
        <div className={styles.root}>
            <Step title="Verkennen">
                Samen met jou gaan we op zoek naar de beste manier jouw project tot een succes te
                maken.
            </Step>
            <Step title="Ontwerpen">
                Wij maken ontwerpen om iets gemakkelijker te maken. We vinden het geweldig om voor
                een complexe opdracht een simpele oplossing te vinden.
            </Step>
            <Step title="Development">
                We gebruiken de nieuwste technologieën en bouwen efficient en flexibel, zodat het
                ook voor jou te begrijpen is.
            </Step>
            <Step title="Verbeteren">
                Een ontwerp is nooit af en we willen altijd helpen om te zoeken naar verbetering.
            </Step>
        </div>
    );
};
