import styles from "../../styles/5-components/header.module.scss";
import { Menu } from "./menu/menu";
import { Logo } from "./logo";
import { MenuHelpText } from "./menu/menuHelpText";

export const Header = () => {
    return (
        <header className={styles.root}>
            <Logo />
            <MenuHelpText />
            <Menu />
        </header>
    );
};
