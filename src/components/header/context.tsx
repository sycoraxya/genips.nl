import { createContext, FunctionComponent, PropsWithChildren, useState } from "react";
import Router from "next/router";

interface HeaderContextProps {
    menuIsOpen: boolean;
    toggleMenu: () => void;
}

export const HeaderContext = createContext<HeaderContextProps>({
    menuIsOpen: false,
    toggleMenu: () => {},
});

export const HeaderContextProvider: FunctionComponent<React.PropsWithChildren<PropsWithChildren>> = ({ children }) => {
    const [menuIsOpen, setMenuIsOpen] = useState<boolean>(false);

    return (
        <HeaderContext.Provider
            value={{
                menuIsOpen,
                toggleMenu: () => setMenuIsOpen(!menuIsOpen),
            }}
        >
            {children}
        </HeaderContext.Provider>
    );
};
