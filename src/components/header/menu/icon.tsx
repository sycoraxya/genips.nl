import { FunctionComponent, useContext, useEffect, useState } from "react";
import styles from "../../../styles/5-components/menu/icon.module.scss";
import { HeaderContext } from "../context";
import classnames from "classnames";
import { AppContext } from "../../appContext";

export const MenuIcon: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { menuIsOpen, toggleMenu } = useContext(HeaderContext);
    const { heroColor } = useContext(AppContext);
    const [dataColor, setDataColor] = useState<"dark" | "light">(
        heroColor === "white" || heroColor === "light" ? "dark" : "light",
    );

    const className = classnames(styles.root, { [styles["root--is-open"]]: menuIsOpen });

    useEffect(() => {
        switch (heroColor) {
            case "white":
            case "light":
                setDataColor("dark");
                break;
            case "black":
            case "dark":
                setDataColor("light");
                break;
            case "green":
                if (menuIsOpen) {
                    setDataColor("dark");
                } else {
                    setDataColor("light");
                }
                break;
        }
    }, [menuIsOpen, heroColor]);

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            className={className}
            onClick={toggleMenu}
            data-color={dataColor}
        >
            <g data-lines="main">
                <path d="M 4,6 L 24,6" className={styles.root__line} />
                <path d="M 0,15 L 20,15" className={styles.root__line} />
            </g>
            {heroColor === "black" ||
                (heroColor === "dark" && (
                    <g data-lines="secondary">
                        <path d="M 4,8 L 24,8" className={styles.root__line} />
                        <path d="M 0,17 L 20,17" className={styles.root__line} />
                    </g>
                ))}
            {heroColor === "green" && (
                <g data-lines="green">
                    <path d="M 4,8 L 24,8" className={styles.root__line} />
                    <path d="M 0,17 L 20,17" className={styles.root__line} />
                </g>
            )}
        </svg>
    );
};
