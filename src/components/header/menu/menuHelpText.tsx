import { FunctionComponent, useContext } from "react";
import styles from "../../../styles/5-components/menu/menuHelpText.module.scss";
import { HeaderContext } from "../context";
import { AppContext } from "../../appContext";

export const MenuHelpText: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { toggleMenu } = useContext(HeaderContext);
    const { heroColor } = useContext(AppContext);

    return (
        <span
            className={styles.root}
            onClick={() => toggleMenu()}
            data-color={heroColor === "white" ? "dark" : "light"}
        >
            menu
        </span>
    );
};
