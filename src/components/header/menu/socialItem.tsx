import { FunctionComponent } from "react";
import styles from "../../../styles/5-components/menu/menu.module.scss";
import { AnimatedLink } from "../../animatedLink";

interface SocialItemProps {
    url: string;
    name: string;
}

export const SocialItem: FunctionComponent<React.PropsWithChildren<SocialItemProps>> = ({ url, name }) => {
    return (
        <li className={styles.socialItem} data-before-visible={"true"}>
            <AnimatedLink
                href={url}
                title={name}
                type="light"
                external
                className={styles.socialItem__name}
            />
        </li>
    );
};
