import { FunctionComponent, useContext, useEffect, useRef } from "react";
import { MenuIcon } from "./icon";
import styles from "../../../styles/5-components/menu/menu.module.scss";
import { HeaderContext } from "../context";
import classnames from "classnames";
import { SocialItem } from "./socialItem";
import { AnimatedLink } from "../../animatedLink";
import { useOnClickOutside } from "@sycoraxya/loki";
import { motion, useAnimation, Variants } from "framer-motion";

export const Menu: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { menuIsOpen, toggleMenu } = useContext(HeaderContext);
    const menuClassNames = classnames(styles.root, { [styles["root--is-open"]]: menuIsOpen });
    const backgroundClassNames = classnames(styles.darkenedBackground, {
        [styles["darkenedBackground--is-open"]]: menuIsOpen,
    });
    const menuRef = useRef<HTMLDivElement>();
    const ulRef = useRef<HTMLUListElement>();
    const containerControls = useAnimation();
    const ulControls = useAnimation();

    useOnClickOutside(menuRef, () => (menuIsOpen ? toggleMenu() : null));

    const containerVariants: Variants = {
        in: {
            opacity: 1,
            pointerEvents: "all",
            x: 0,
            transition: {
                duration: 0.2,
                ease: "circOut",
                opacity: {
                    duration: 0.1,
                },
            },
        },
        out: {
            opacity: 0,
            pointerEvents: "none",
            x: "100%",
            transition: {
                duration: 0.2,
                ease: "circIn",
                opacity: {
                    duration: 0.3,
                },
            },
        },
    };

    const ulVariants: Variants = {
        hidden: {},
        show: {
            transition: {
                delay: 0.2,
                staggerChildren: 0.05,
            },
        },
    };

    const liVariants: Variants = {
        hidden: {
            opacity: 0,
            x: 100,
        },
        show: {
            opacity: 1,
            x: 0,
        },
    };

    useEffect(() => {
        const menuItems = Array.from(ulRef.current.children);
        if (menuIsOpen) {
            containerControls.start("in");
            ulControls.start("show");

            menuItems.forEach((child, index) => {
                setTimeout(() => {
                    child.setAttribute("data-before-visible", "true");
                }, (index + 1) * 50 + 400);
            });
        } else {
            containerControls.start("out");
            ulControls.start("hidden");

            menuItems.forEach((child) => {
                child.setAttribute("data-before-visible", "false");
            });
        }
    }, [menuIsOpen, containerControls, ulControls]);

    return (
        <>
            <div className={backgroundClassNames} />
            <div className={styles.wrapper} ref={menuRef}>
                <MenuIcon />
                <motion.div
                    className={menuClassNames}
                    initial={{ opacity: 0, x: "100%" }}
                    animate={containerControls}
                    variants={containerVariants}
                >
                    <motion.ul
                        className={styles.items}
                        animate={ulControls}
                        initial="hidden"
                        variants={ulVariants}
                        ref={ulRef}
                    >
                        <motion.li className={styles.item} variants={liVariants}>
                            <AnimatedLink href="/" title={"Home"} type="light" />
                        </motion.li>
                        <motion.li className={styles.item} variants={liVariants}>
                            <AnimatedLink href="/services" title={"Services"} type="light" />
                        </motion.li>
                        <motion.li className={styles.item} variants={liVariants}>
                            <AnimatedLink href="/over-ons" title={"Over ons"} type="light" />
                        </motion.li>
                        <motion.li className={styles.item} variants={liVariants}>
                            <AnimatedLink
                                href="/duurzaamheid"
                                title={"Duurzaamheid"}
                                type="light"
                            />
                        </motion.li>
                        <motion.li className={styles.item} variants={liVariants}>
                            <AnimatedLink href="/contact" title={"Contact"} type="light" />
                        </motion.li>
                    </motion.ul>

                    <ul className={styles.social}>
                        <SocialItem url="https://www.linkedin.com/company/genips" name="LinkedIn" />
                        <SocialItem url="https://www.facebook.com/genipsagency/" name="Facebook" />
                    </ul>
                </motion.div>
            </div>
        </>
    );
};
