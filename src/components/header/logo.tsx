import { FunctionComponent, useContext } from "react";
import Link from "next/link";
import styles from "../../styles/5-components/header.module.scss";
import classnames from "classnames";
import { AppContext } from "../appContext";

export enum Color {
    Blue = "/genips-logo-black.svg",
    White = "/genips-logo-white.svg",
}

interface LogoProps {
    color?: Color;
    className?: string;
}

export const Logo: FunctionComponent<React.PropsWithChildren<LogoProps>> = ({
    className = false,
}) => {
    const { heroColor } = useContext(AppContext);
    const classNames = classnames(className);

    const color =
        heroColor === "black" || heroColor === "dark" || heroColor === "green"
            ? Color.White
            : Color.Blue;

    return (
        <Link href="/" scroll={false} className={classNames}>
            <img src={color} alt="Genips Logo" className={styles.logo} />
        </Link>
    );
};
