import { FunctionComponent, PropsWithChildren, useContext, useEffect } from "react";
import { initGa, logPageView } from "../utils/analytics";
import { motion } from "framer-motion";
import classnames from "classnames";
import { useRouter } from "next/router";
import { AppContext } from "./appContext";
import { HeaderContext } from "./header/context";
import { usePageTransitionTimeout } from "../hooks/usePageTransitionTimeout";

interface LayoutProps {
    overlayColor?: "white";
    overlay?: boolean;
}

export const Layout: FunctionComponent<React.PropsWithChildren<PropsWithChildren<LayoutProps>>> = ({
    children,
    overlayColor,
    overlay,
}) => {
    useEffect(() => {
        // @ts-ignore
        if (!window.GA_INITIALIZED) {
            initGa();
            // @ts-ignore
            window.GA_INITIALIZED = true;
        }

        logPageView();
    }, []);
    const className = classnames("overlay", {
        "overlay--white": overlayColor === "white",
    });

    // const { isClientRouting, setIsClientRouting } = useContext(AppContext);
    const { menuIsOpen, toggleMenu } = useContext(HeaderContext);
    // const router = useRouter();

    usePageTransitionTimeout(() => window.scrollTo(0, 0));

    // Close menu while transitioning page
    usePageTransitionTimeout(() => (menuIsOpen ? toggleMenu() : null), [menuIsOpen]);

    /* useEffect(() => {
        if (isClientRouting) {
            return;
        }

        const handleStart = (url: string) => url !== router.pathname && setIsClientRouting(true);

        router.events.on("routeChangeStart", handleStart);

        return () => {
            router.events.off("routeChangeStart", handleStart);
        };
    }); */

    return (
        <main>
            {/* <motion.div
                className="fullColorOverlay"
                variants={{
                    initial: {
                        opacity: 1,
                        width: "100vw",
                        height: "100vh",
                        right: 0,
                        top: 0,
                        x: "0%",
                    },
                    animateOut: {
                        x: isClientRouting ? ["0%", "-100%"] : ["100%", "100%"],
                        transition: {
                            duration: 0.6,
                            times: [0, 0.5],
                            ease: "easeOut",
                        },
                    },
                    animateIn: {
                        x: ["100%", "0%"],
                        transition: {
                            duration: 0.6,
                            times: [0, 0.5],
                            ease: "easeIn",
                        },
                    },
                }}
                initial="initial"
                animate="animateOut"
                exit="animateIn"
            ></motion.div>
            {!isClientRouting && (
                <motion.div
                    className={className}
                    initial={{ opacity: 1, zIndex: 90 }}
                    animate={{
                        opacity: 0,
                        transition: {
                            delay: 0.2,
                            duration: 0.2,
                            ease: "easeInOut",
                        },
                        transitionEnd: {
                            zIndex: 0,
                        },
                    }}
                ></motion.div>
            )} */}
            {children}
        </main>
    );
};
