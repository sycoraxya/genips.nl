import { AnimatePresence, motion } from "framer-motion";
import React, { FunctionComponent, PropsWithChildren } from "react";
import styles from "../../styles/5-components/form/submit.module.scss";
import spinnerStyles from "../../styles/5-components/form/spinnerGrow.module.scss";

interface SubmitButtonProps {
    success?: boolean;
    layoutId?: string;
    isLoading?: boolean;
}

export const Submit: FunctionComponent<React.PropsWithChildren<PropsWithChildren<SubmitButtonProps>>> = ({
    success = false,
    children,
    layoutId,
    isLoading = false,
}) => {
    return (
        <motion.button
            type="submit"
            className={styles.root}
            disabled={success || isLoading}
            layoutId={layoutId}
            layout
            animate={{ backgroundColor: success ? "#87c8d3" : "#000000" }}
            transition={{ duration: 0.1 }}
        >
            {children}
            <AnimatePresence>
                {isLoading && (
                    <motion.div
                        className={spinnerStyles.root}
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }}
                        transition={{ duration: 0.2 }}
                    >
                        <span className={spinnerStyles.spinner} />
                    </motion.div>
                )}
            </AnimatePresence>
        </motion.button>
    );
};
