import Link from "next/link";
import styles from "../styles/5-components/footer.module.scss";

export const Footer = () => {
    const date = new Date();

    return (
        <footer className={styles.root}>
            <div className={styles.container}>
                <section className={styles.section}>
                    <img src="/genips-logo-blue.svg" alt="Genips Logo" className={styles.logo} />
                </section>
                <section className={styles.section}>
                    <span className={styles.heading}>Contact —</span>
                    <ul>
                        <li>
                            <a
                                href="mailto:hallo@genips.nl"
                                onClick={() =>
                                    //@ts-ignore
                                    window.gtag("event", "Click", {
                                        category: "Contact",
                                        label: "Clicked email address footer",
                                    })
                                }
                            >
                                hallo@genips.nl
                            </a>
                        </li>
                        <li>
                            <a
                                href="tel:+31 6 254 630 10"
                                onClick={() =>
                                    //@ts-ignore
                                    window.gtag("event", "Click", {
                                        category: "Contact",
                                        label: "Clicked phone number footer",
                                    })
                                }
                            >
                                +31 6 254 630 10
                            </a>
                        </li>
                    </ul>
                </section>

                <section className={styles.section}>
                    <span className={styles.heading}>Social —</span>
                    <ul>
                        <li>
                            <a
                                href="https://www.linkedin.com/company/genips"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Linkedin
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.facebook.com/genipsagency/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Facebook
                            </a>
                        </li>
                    </ul>
                </section>

                <section className={styles.section}>
                    <span className={styles.heading}>Genips B.V. —</span>
                    <ul>
                        <li>Meidoornhof 19</li>
                        <li>3831 XR Leusden</li>
                        <li>KVK 92267114</li>
                        <li>BTW-ID NL865965766B01</li>
                    </ul>
                </section>

                <section className={styles["bottom-bar"]}>
                    <span className={styles.copyright}>
                        © {date.getFullYear()} Genips. Alle rechten voorbehouden.
                    </span>

                    <span className={styles.terms}>
                        <Link href="/algemene-voorwaarden" target="_blank">
                            Algemene voorwaarden
                        </Link>
                        <span className={styles.divider} />
                        <a
                            href="https://status.genips.nl"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Server Status
                        </a>
                        <span className={styles.divider} />
                        <a href="mailto:support@genips.nl">Support</a>
                    </span>
                </section>
            </div>
        </footer>
    );
};
