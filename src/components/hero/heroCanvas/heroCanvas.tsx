import { FunctionComponent, useRef, useEffect } from "react";
import heroStyles from "../../styles/5-components/home/hero.module.scss";
import { handleClick, setCanvas } from "./helpers";

export const HeroCanvas: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const canvasRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        const canvas = canvasRef.current;
        setCanvas(canvas);
    }, []);

    return <canvas className={heroStyles.canvas} ref={canvasRef} onClick={handleClick}></canvas>;
};
