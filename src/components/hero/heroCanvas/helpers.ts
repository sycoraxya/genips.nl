import anime from "animejs";
import react from "react";

let animations: anime.AnimeInstance[] = [];
let cH: number;
let cW: number;
let bgColor = "#2d3436";
let canvas: HTMLCanvasElement;
let ctx: CanvasRenderingContext2D;

export const setCanvas = (canvasToSet: HTMLCanvasElement) => {
    canvas = canvasToSet;
    ctx = canvas.getContext("2d");

    const resizeCanvas = () => {
        const bcr = canvas.getBoundingClientRect();
        cW = bcr.width;
        cH = bcr.height;
        canvas.width = cW * devicePixelRatio;
        canvas.height = cH * devicePixelRatio;
        ctx.scale(devicePixelRatio, devicePixelRatio);
    };

    resizeCanvas();

    window.addEventListener("resize", resizeCanvas);

    const animate = anime({
        duration: Infinity,
        update: function () {
            ctx.fillStyle = bgColor;
            ctx.fillRect(0, 0, cW, cH);
            animations.forEach((anim) => {
                anim.animatables.forEach((animatable) => {
                    // @ts-ignore
                    animatable.target.draw();
                });
            });
        },
    });
};

export const colorPicker = (() => {
    const colors = ["#2D3436", "#F6BC53", "#6C63FF"];
    let index = 0;

    const next = () => {
        index = index++ < colors.length - 1 ? index : 0;

        return colors[index];
    };

    const current = () => {
        return colors[index];
    };

    return { next, current };
})();

export const removeAnimation = (animation: anime.AnimeInstance) => {
    const index = animations.indexOf(animation);

    if (index > -1) {
        animations.splice(index, 1);
    }
};

export const calcPageFillRadius = (x: number, y: number) => {
    const l = Math.max(x - 0, cW - x);
    const h = Math.max(y - 0, cH - y);

    return Math.sqrt(Math.pow(l, 2) + Math.pow(h, 2));
};

export const handleClick = (event: react.MouseEvent<HTMLCanvasElement, MouseEvent>) => {
    // @ts-ignore
    if (event.touches) {
        event.preventDefault();
        // @ts-ignore
        event = event.touches[0];
    }

    const currentColor = colorPicker.current();
    const nextColor = colorPicker.next();
    const targetR = calcPageFillRadius(event.pageX, event.pageY);
    const rippleSize = Math.min(200, cW * 0.4);
    const minCoverDuration = 750;

    const pageFill = new Circle({
        x: event.pageX,
        y: event.pageY,
        r: 0,
        fill: nextColor,
    });

    const fillAnimation = anime({
        targets: pageFill,
        r: targetR,
        duration: Math.max(targetR / 2, minCoverDuration),
        easing: "easeOutQuart",
        complete: () => {
            bgColor = pageFill.fill;
            removeAnimation(fillAnimation);
        },
    });

    var ripple = new Circle({
        x: event.pageX,
        y: event.pageY,
        r: 0,
        fill: currentColor,
        stroke: {
            width: 3,
            color: currentColor,
        },
        opacity: 1,
    });
    var rippleAnimation = anime({
        targets: ripple,
        r: rippleSize,
        opacity: 0,
        easing: "easeOutExpo",
        duration: 900,
        complete: removeAnimation,
    });

    const particles = [];

    for (var i = 0; i < 32; i++) {
        const particle = new Circle({
            x: event.pageX,
            y: event.pageY,
            fill: currentColor,
            r: anime.random(24, 48),
        });

        particles.push(particle);
    }

    const particlesAnimation = anime({
        targets: particles,
        x: (particle: Circle) => particle.x + anime.random(rippleSize, -rippleSize),
        y: (particle: Circle) => particle.y + anime.random(rippleSize * 1.15, -rippleSize * 1.15),
        r: 0,
        easing: "easeOutExpo",
        duration: anime.random(1000, 1300),
        complete: removeAnimation,
    });

    animations.push(fillAnimation, rippleAnimation, particlesAnimation);
};

export class Circle {
    private readonly opacity: number;
    private readonly stroke: {
        width: number;
        color: string;
    };
    private readonly r: number;
    public readonly fill: string;
    public readonly x: number;
    public readonly y: number;

    constructor(opts: any) {
        for (let key in opts) {
            if (opts.hasOwnProperty(key)) {
                this[key] = opts[key];
            }
        }
    }

    public draw() {
        ctx.globalAlpha = this.opacity || 1;
        ctx.beginPath();

        ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, false);

        if (this.stroke) {
            ctx.strokeStyle = this.stroke.color;
            ctx.lineWidth = this.stroke.width;
            ctx.stroke();
        }
        if (this.fill) {
            ctx.fillStyle = this.fill;
            ctx.fill();
        }

        ctx.closePath();

        ctx.globalAlpha = 1;
    }
}
