import { FunctionComponent, HTMLAttributes, MutableRefObject } from "react";
import classnames from "classnames";
import styles from "../../styles/5-components/hero/hero.module.scss";

interface HeroProps extends HTMLAttributes<HTMLDivElement> {
    image?: {
        src: string;
        alt: string;
        ref?: MutableRefObject<HTMLImageElement>;
    };
    color?: "black" | "white";
}

export const Hero: FunctionComponent<React.PropsWithChildren<HeroProps>> = ({ image, color, children, ...props }) => {
    const containerClasses = classnames({
        [styles.container]: true,
        [styles["container--has-image"]]: image,
    });
    const imageClasses = classnames({
        [styles.willAnimate]: image && image.ref,
    });

    return (
        <section {...props} className={styles.root} data-color={color}>
            <div className={containerClasses}>
                {image && (
                    <div className={styles.image}>
                        <img
                            src={image.src}
                            alt={image.alt}
                            className={imageClasses}
                            ref={image.ref || null}
                        />
                    </div>
                )}
                {children}
            </div>
        </section>
    );
};
