import React, { FunctionComponent, ReactNode } from "react";
import styles from "../styles/5-components/portfolioItem.module.scss";
import { ChevronRight } from "./chevronRight";

export enum Tag {
    WordPress = "WordPress",
    WooCommerce = "WooCommerce",
    Elementor = "Elementor",
    Nextjs = "Next.js",
    React = "React",
    TypeScript = "TypeScript",
    Nestjs = "Nest.js",
    Nodejs = "Node.js",
    Svelte = "Svelte",
}

interface PortfolioItemProps {
    category: string;
    title: string;
    subtitle: string;
    tags: Tag[];
    url?: string;
    image: string;
    images?: { media: string; srcSet: string }[];
    children: ReactNode;
    id: string;
    imagePosition?: "left" | "right";
    buttonText?: string;
}

export const PortfolioItem: FunctionComponent<React.PropsWithChildren<PortfolioItemProps>> = ({
    category,
    title,
    subtitle,
    tags,
    url,
    image,
    images,
    children,
    id,
    imagePosition = "left",
    buttonText = "Ontdek de website",
}) => {
    return (
        <section className={styles.root} data-project={id} data-image-position={imagePosition}>
            <div className={styles.imageWrapper}>
                <picture>
                    {images &&
                        images.map((imgDef, index) => (
                            <source
                                key={`portfolio_img_${title}_${index}`}
                                media={imgDef.media}
                                srcSet={imgDef.srcSet}
                            />
                        ))}
                    <img
                        loading="lazy"
                        src={image}
                        alt={`Project ${title}`}
                        title={`Project ${title}`}
                    />
                </picture>
            </div>
            <div className={styles.content}>
                <span className={styles.date}>{category}</span>
                <div className={styles.vCenter}>
                    <div className={styles.title}>{title}</div>
                    <div className={styles.subtitle}>{subtitle}</div>
                    {children}
                    <div className={styles.tags}>{tags.join(", ")}</div>
                    {url && (
                        <a
                            href={url}
                            target="_blank"
                            rel="noopener noreferrer"
                            className={styles.button}
                        >
                            {buttonText}
                            <ChevronRight size={14} />
                        </a>
                    )}
                </div>
            </div>
        </section>
    );
};
