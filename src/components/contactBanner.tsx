import { FunctionComponent } from "react";
import styles from "../styles/5-components/contactBanner.module.scss";

interface ContactBannerProps {
    heading?: string;
    gaPageName?: string;
}

export const ContactBanner: FunctionComponent<React.PropsWithChildren<ContactBannerProps>> = ({ heading, gaPageName }) => {
    return (
        <section className={styles.root}>
            <h4 className={styles.root__heading}>{heading || "Stuur ons een bericht"}</h4>
            <h3 className={styles.root__email}>
                <a
                    href="mailto:hallo@genips.nl"
                    onClick={() =>
                        //@ts-ignore
                        window.gtag("event", "Click", {
                            category: "Contact",
                            label: `Clicked email address ${gaPageName || "about us"} page`,
                        })
                    }
                    className={styles.emailUrl}
                >
                    hallo@genips.nl
                </a>
            </h3>
        </section>
    );
};
