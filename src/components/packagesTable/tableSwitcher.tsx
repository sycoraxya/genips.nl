import React, { FunctionComponent, useState } from "react";
import styles from "../../styles/5-components/packagesTable/tableSwitcher.module.scss";

interface TableSwitcherProps {
    values: { key: string; display: string }[];
    callback: (active: any) => void;
    active?: string;
}

export const TableSwitcher: FunctionComponent<React.PropsWithChildren<TableSwitcherProps>> = ({
    callback,
    values,
    active: activeValue,
}) => {
    const [active, setActive] = useState(activeValue || values[0].key);

    const handleSetActive = (active: string) => {
        setActive(active);
        callback(active);
    };

    return (
        <section className={styles.root} data-value-count={values.length}>
            {values.map(({ key, display }) => (
                <div
                    key={key}
                    onClick={() => handleSetActive(key)}
                    className={styles.value}
                    data-is-active={active === key}
                >
                    {display}
                </div>
            ))}
        </section>
    );
};
