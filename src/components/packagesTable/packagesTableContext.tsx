import { createContext, FunctionComponent, PropsWithChildren, useState } from "react";
import { Package, TableRows, PackageType } from "./packagesTable";

export interface PackagesTableContextProps {
    packages: Package[];
    rows: TableRows;
    selected: PackageType;
    setSelected?: (pkg: PackageType) => void;
}

export const PackagesTableContext = createContext<PackagesTableContextProps>({
    packages: [],
    rows: [],
    selected: null,
    setSelected: () => null,
});

export const PackagesTableContextProvider: FunctionComponent<React.PropsWithChildren<PropsWithChildren<PackagesTableContextProps>>> = ({ children, packages, selected, rows }) => {
    const [selectedPackage, setSelectedPackage] = useState<PackageType>(selected);

    return (
        <PackagesTableContext.Provider
            value={{
                packages,
                rows,
                selected: selectedPackage,
                setSelected: (pkg: PackageType) => {
                    setSelectedPackage(pkg);
                },
            }}
        >
            {children}
        </PackagesTableContext.Provider>
    );
};
