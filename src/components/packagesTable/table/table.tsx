import { FunctionComponent } from "react";
import { TableHead } from "./tableHead";
import { TableBody } from "./tableBody";

export const Table: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    return (
        <table>
            <TableHead />
            <TableBody />
        </table>
    );
};
