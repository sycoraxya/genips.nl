import React, { FunctionComponent, useContext } from "react";
import { PackageRow, TableRow } from "../packagesTable";
import styles from "../../../styles/5-components/packagesTable/tableBody.module.scss";
import { PackagesTableContext } from "../packagesTableContext";
import { CircleCheck } from "../../checkmark";
import { CircleCross } from "../../cross";

interface TableBodyRowProps {
    row: TableRow;
}

export const TableBodyRow: FunctionComponent<React.PropsWithChildren<TableBodyRowProps>> = ({ row }) => {
    const { packages } = useContext(PackagesTableContext);

    return (
        <tr className={styles.row}>
            <td className={styles.row__title} data-has-description={!!row.description}>
                {row.key}
                {row.description && <span className={styles.tooltip}>{row.description}</span>}
            </td>
            {packages.map((pkg, index) => {
                const packageMap = new Map<
                    PackageRow["key"],
                    { value: PackageRow["value"]; subValue?: PackageRow["subValue"] }
                >(
                    pkg.rows.map((val) => {
                        return [
                            val.key,
                            {
                                value: val.value,
                                subValue: val.subValue,
                            },
                        ];
                    }),
                );
                let value = packageMap.get(row.key)?.value;
                let subValue = packageMap.get(row.key)?.subValue;
                let icon: JSX.Element;

                if (typeof value === "boolean") {
                    icon = value ? <CircleCheck /> : <CircleCross />;
                }

                if (Array.isArray(value)) {
                    return (
                        <td key={`${row.key}_${pkg.type}_${index}`} className={styles.row__cell}>
                            <ul>
                                {value.map((singleValue, index) => (
                                    <li key={`${row.key}_${pkg.type}_${singleValue}`}>
                                        {singleValue}
                                    </li>
                                ))}
                            </ul>
                        </td>
                    );
                }

                return (
                    <td key={`${row.key}_${pkg.type}_${index}`} className={styles.row__cell}>
                        {icon ? icon : value}
                        {subValue && <span className={styles.subValue}>{subValue}</span>}
                    </td>
                );
            })}
        </tr>
    );
};
