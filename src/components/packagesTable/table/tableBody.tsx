import React, { FunctionComponent, useContext } from "react";
import { TableHeading, TableRow } from "../packagesTable";
import styles from "../../../styles/5-components/packagesTable/tableBody.module.scss";
import { PackagesTableContext } from "../packagesTableContext";
import { TableBodyRow } from "./tableBodyRow";

export const TableBody: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { packages, rows } = useContext(PackagesTableContext);
    const isRow = (row: any): row is TableRow => row.key !== undefined;
    const isHeading = (row: any): row is TableHeading => row.type === "heading";

    return (
        <tbody>
            {rows.map((row) => {
                if (typeof row !== "string") {
                    if (isHeading(row)) {
                        // Return heading element
                        // Can't use colspan for this td because it fucks up borders
                        return (
                            <tr key={row.value} className={`${styles.row}`} data-type="heading">
                                <td className={styles.row__heading} colSpan={packages.length + 1}>
                                    {row.value}
                                </td>
                            </tr>
                        );
                    }
                }

                const thisRow: TableRow = typeof row === "string" ? { key: row } : row;
                return <TableBodyRow row={thisRow} key={thisRow.key} />;
            })}
        </tbody>
    );
};
