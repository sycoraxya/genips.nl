import { FunctionComponent, useContext } from "react";
import { PackageType } from "../packagesTable";
import styles from "../../../styles/5-components/packagesTable/tableHead.module.scss";
import { PackagesTableContext } from "../packagesTableContext";

export const TableHead: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { packages } = useContext(PackagesTableContext);

    return (
        <thead className={styles.root}>
            <tr>
                <th className={styles.root__fill}></th>
                {packages.map((pkg) => {
                    return (
                        <th key={pkg.type} className={styles.root__cell}>
                            <div className={styles.root__cellContent}>
                                <span className={styles.package__name}>
                                    {PackageType[pkg.type].replace("_", " ")}
                                </span>
                                {pkg.price && (
                                    <span className={styles.package__pricing}>
                                        <span className={styles.package__price}>
                                            {pkg.price % 1 === 0
                                                ? `${pkg.price},-`
                                                : pkg.price.toLocaleString("nl-NL", {
                                                      minimumFractionDigits: 2,
                                                  })}
                                        </span>
                                        <span className={styles.package__term}>per maand</span>
                                    </span>
                                )}
                                {pkg.description && (
                                    <span className={styles.package__description}>
                                        {pkg.description}
                                    </span>
                                )}
                            </div>
                        </th>
                    );
                })}
            </tr>
        </thead>
    );
};
