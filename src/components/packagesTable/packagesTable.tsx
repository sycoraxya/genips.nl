import { FunctionComponent, ReactNode } from "react";
import styles from "../../styles/5-components/packagesTable/packagesTable.module.scss";
import { useWindowWidth } from "@react-hook/window-size";
import { Table } from "./table/table";
import { Mobile } from "./mobile/mobile";
import { PackagesTableContextProvider } from "./packagesTableContext";

export enum PackageType {
    Start,
    Professional,
    Premium,
    Performance,
    Oude_Prijs,
    Jaarlijks,
    Basic,
    Business,
    Grow,
    Scale,
}

export interface PackageRow {
    key: string | number;
    value: string | boolean | string[];
    subValue?: string;
}

export type TableRows = Array<string | TableHeading | TableRow>;

export interface TableRow {
    key: string;
    description?: string;
}

export interface TableHeading {
    type: "heading";
    value: string;
}

export interface Package {
    type: PackageType;
    price?: number;
    description?: string;
    rows: PackageRow[];
}

interface PackagesTableProps {
    packages: Package[];
    rows: TableRows;
}

export const PackagesTable: FunctionComponent<React.PropsWithChildren<PackagesTableProps>> = ({
    packages,
    rows,
}) => {
    const windowWidth = useWindowWidth();

    const selected = packages[0].type;

    return (
        <PackagesTableContextProvider packages={packages} rows={rows} selected={selected}>
            <section
                className={styles.root}
                suppressHydrationWarning={true}
                data-col-count={packages.length}
            >
                {windowWidth > 1024 ? <Table /> : <Mobile />}
            </section>
        </PackagesTableContextProvider>
    );
};
