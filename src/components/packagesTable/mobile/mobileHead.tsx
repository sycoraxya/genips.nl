import { FunctionComponent, useContext } from "react";
import { PackageType } from "../packagesTable";
import styles from "../../../styles/5-components/packagesTable/mobileHead.module.scss";
import { PackagesTableContext } from "../packagesTableContext";

export const MobileHead: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { packages, selected } = useContext(PackagesTableContext);
    const pkg = packages.find((pkg) => pkg.type === selected);

    return (
        <div className={styles.root}>
            <span className={styles.package__name}>{PackageType[pkg.type].replace("_", " ")}</span>
            {pkg.price && (
                <span className={styles.package__pricing}>
                    <span className={styles.package__price}>
                        {pkg.price % 1 === 0
                            ? `${pkg.price},-`
                            : pkg.price.toLocaleString("nl-NL", {
                                  minimumFractionDigits: 2,
                              })}
                    </span>
                    <span className={styles.package__term}>per maand</span>
                </span>
            )}
            {pkg.description && (
                <span className={styles.package__description}>{pkg.description}</span>
            )}
        </div>
    );
};
