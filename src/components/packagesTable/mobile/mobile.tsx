import { FunctionComponent } from "react";
import { MobileHead } from "./mobileHead";
import { MobileBody } from "./mobileBody";
import { PackageSelector } from "./packageSelector";

export const Mobile: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    return (
        <div>
            <PackageSelector />
            <MobileHead />
            <MobileBody />
        </div>
    );
};
