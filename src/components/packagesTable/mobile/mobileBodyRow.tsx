import React, { FunctionComponent, useContext } from "react";
import { CircleCheck } from "../../checkmark";
import { CircleCross } from "../../cross";
import { PackageRow, TableRow } from "../packagesTable";
import { PackagesTableContext } from "../packagesTableContext";
import styles from "../../../styles/5-components/packagesTable/mobileBody.module.scss";

interface MobileBodyRowProps {
    row: TableRow;
}

export const MobileBodyRow: FunctionComponent<React.PropsWithChildren<MobileBodyRowProps>> = ({ row }) => {
    const { packages, selected } = useContext(PackagesTableContext);
    const pkg = packages.find((pkg) => pkg.type === selected);
    const packageMap = new Map<
        PackageRow["key"],
        { value: PackageRow["value"]; subValue?: PackageRow["subValue"] }
    >(
        pkg.rows.map((val) => {
            return [
                val.key,
                {
                    value: val.value,
                    subValue: val.subValue,
                },
            ];
        }),
    );
    let value = packageMap.get(row.key)?.value;
    let subValue = packageMap.get(row.key)?.subValue;
    let icon: JSX.Element;

    if (typeof value === "boolean") {
        icon = value ? <CircleCheck /> : <CircleCross />;
    }

    if (Array.isArray(value)) {
        return (
            <div key={row.key} className={styles.root__row}>
                <span className={styles.root__title}>{row.key}</span>
                <ul>
                    {value.map((singleValue, index) => (
                        <li key={`${row}_${pkg.type}_${singleValue}`}>{singleValue}</li>
                    ))}
                </ul>
            </div>
        );
    }

    return (
        <div key={row.key} className={styles.root__row}>
            <span className={styles.root__title}>{row.key}</span>
            <span className={styles.root__value}>{icon ? icon : value}</span>
            {subValue && <span className={styles.subValue}>{subValue}</span>}
        </div>
    );
};
