import { FunctionComponent, useContext } from "react";
import { PackagesTableContext } from "../packagesTableContext";
import { PackageType } from "../packagesTable";
import styles from "../../../styles/5-components/packagesTable/packageSelector.module.scss";

export const PackageSelector: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { packages, selected, setSelected } = useContext(PackagesTableContext);

    return (
        <div className={styles.root}>
            {packages.map((pkg) => {
                return (
                    <span
                        key={`${pkg.type}_selector`}
                        className={styles.package}
                        {...(selected === pkg.type ? { "data-active": true } : {})}
                        onClick={() => setSelected(pkg.type)}
                    >
                        {PackageType[pkg.type].replace("_", " ")}
                    </span>
                );
            })}
        </div>
    );
};
