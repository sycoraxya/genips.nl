import React, { FunctionComponent, useContext } from "react";
import { TableHeading, TableRow } from "../packagesTable";
import styles from "../../../styles/5-components/packagesTable/mobileBody.module.scss";
import { PackagesTableContext } from "../packagesTableContext";
import { MobileBodyRow } from "./mobileBodyRow";

export const MobileBody: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { rows } = useContext(PackagesTableContext);
    const isHeading = (row: any): row is TableHeading => row.type === "heading";

    return (
        <div className={styles.root}>
            {rows.map((row) => {
                if (typeof row !== "string") {
                    if (isHeading(row)) {
                        // Return heading element
                        return (
                            <div key={row.value} className={styles.root__row} data-type="heading">
                                {row.value}
                            </div>
                        );
                    }
                }

                const thisRow: TableRow = typeof row === "string" ? { key: row } : row;
                return <MobileBodyRow row={thisRow} key={thisRow.key} />;
            })}
        </div>
    );
};
