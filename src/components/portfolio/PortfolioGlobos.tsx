import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioGlobos: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"b2b e-commerce"}
        tags={[Tag.WordPress, Tag.WooCommerce, Tag.WordPress]}
        title={"Globos Europe"}
        subtitle={"Gemakkelijk bestellen bij de 'groothandel' in partyspullen"}
        url={""}
        image={"/portfolio/globos.jpg"}
        id="globos"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Binnenkort beschikbaar"
    >
        <p>
            <ul>
                <li>API koppeling met Uniconta</li>
                <li>Geavanceerde AI-powered meertaligheid</li>
                <li>Infrastructuur met AWS Lightsail & CloudFront</li>
                <li>Speciale B2B features, zoals prijslijsten & .CSV imports</li>
            </ul>
        </p>
    </PortfolioItem>
);
