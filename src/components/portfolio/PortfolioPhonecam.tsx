import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioPhonecam: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"webapplicatie"}
        tags={[Tag.Svelte]}
        title={"PhoneCam"}
        subtitle={"Al het overzicht in een next-gen webapp"}
        url={"https://phonecam.ai"}
        image={"/portfolio/phonecam.jpg"}
        id="phonecam"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Lees meer over PhoneCam"
    >
        <p>
            <ul>
                <li>Van ontwerp tot ontwikkeling</li>
                <li>API koppeling met Android & IOS app</li>
                <li>Firebase realtime database</li>
            </ul>
        </p>
    </PortfolioItem>
);
