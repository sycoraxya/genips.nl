import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioHaasF1: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"b2c e-commerce"}
        tags={[Tag.WordPress, Tag.WooCommerce, Tag.Elementor]}
        title={"MoneyGram Haas F1 Team"}
        subtitle={"Snelle merchandise shop voor racers"}
        url={"https://haasf1team.tricorp.com"}
        image={"/portfolio/haasf1.jpg"}
        id="haasf1"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek de webshop"
    >
        <p>
            <ul>
                <li>API koppeling met Uniconta</li>
                <li>A/B en multivariant testen voor conversie optimalisatie</li>
                <li>Geavanceerde AI-powered meertaligheid</li>
                <li>Snelheidsoptimalisatie met Lightspeed & een CDN</li>
                <li>Veel (internationale) afrekenmogelijkheden via onze betaalpartner Pay</li>
            </ul>
        </p>
    </PortfolioItem>
);
