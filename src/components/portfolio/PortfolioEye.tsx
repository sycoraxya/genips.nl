import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioEye: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"webapplicatie"}
        tags={[Tag.Nestjs, Tag.React, Tag.TypeScript]}
        title={"EYE Security"}
        subtitle={"Slimme onboarding voor nieuwe klanten"}
        url={"https://www.eye.security/nl"}
        image={"/portfolio/eye.jpg"}
        id="eye"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek meer over EYE Security"
    >
        <p>
            <ul>
                <li>Van ontwerp tot ontwikkeling</li>
                <li>Authenticatie via Auth0</li>
                <li>API koppeling met Hubspot</li>
            </ul>
        </p>
    </PortfolioItem>
);
