import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioPromentum: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"Promentum Consulting"}
        subtitle={"Overzichtelijke en professionele website"}
        url={"https://promentum-consulting.nl"}
        image={"/portfolio/promentum.jpg"}
        id="promentum"
        imagePosition={imagePositionOverride || "left"}
        buttonText="Ontdek de website"
    >
        <p>
            <ul>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Geavanceerde AI-powered meertaligheid</li>
                <li>Complete marketingstrategie</li>
                <li>Social Media marketing</li>
            </ul>
        </p>
    </PortfolioItem>
);
