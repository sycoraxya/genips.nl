import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioLuna: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"b2b e-commerce"}
        tags={[Tag.WordPress, Tag.WooCommerce, Tag.Elementor]}
        title={"Luna Electronic"}
        subtitle={"Meer dan 5.000 producten in 1 seconde beschikbaar"}
        url={"https://luna-electronic.de"}
        image={"/portfolio/luna.jpg"}
        id="luna"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek de webshop"
    >
        <p>
            <ul>
                <li>API koppeling met Uniconta</li>
                <li>Geavanceerde AI-powered meertaligheid</li>
                <li>Infrastructuur met AWS Lightsail</li>
            </ul>
        </p>
    </PortfolioItem>
);
