import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioBasicproducts: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"webapplicatie"}
        tags={[Tag.Nextjs, Tag.TypeScript]}
        title={"Basic Products"}
        subtitle={"Gemakkelijk als bedrijf je cadeau-artikelen bestellen"}
        url={"https://basicproducts.nl"}
        image={"/portfolio/basicproducts.jpg"}
        id="basicproducts"
        imagePosition={imagePositionOverride || "left"}
        buttonText="Ontdek de webshop"
    >
        <p>
            <ul>
                <li>Tailwind UI als frontend</li>
                <li>Van bestellen tot administratief beheer</li>
                <li>Automatische marketing met nieuwsbrieven</li>
            </ul>
        </p>
    </PortfolioItem>
);
