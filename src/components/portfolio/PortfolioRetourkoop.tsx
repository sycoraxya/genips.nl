import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioRetourkoop: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"b2c e-commerce"}
        tags={[Tag.WordPress, Tag.WooCommerce, Tag.Elementor]}
        title={"Retourkoop"}
        subtitle={"Duurzame verkoop van gebruikte producten"}
        url={"https://retourkoop.nl"}
        image={"/portfolio/retourkoop.jpg"}
        id="retourkoop"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek de webshop"
    >
        <p>
            <ul>
                <li>API koppeling met Microsoft PowerApps</li>
                <li>A/B testen voor conversie optimalisatie</li>
                <li>Geavanceerde AI-powered meertaligheid</li>
                <li>Snelheidsoptimalisatie met o.a. AWS CloudFront</li>
                <li>Veel afrekenmogelijkheden via onze betaalpartner Pay</li>
            </ul>
        </p>
    </PortfolioItem>
);
