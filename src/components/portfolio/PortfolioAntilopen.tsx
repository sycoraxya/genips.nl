import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioAntilopen: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"Korfbalclub Antilopen"}
        subtitle={"WordPress website met externe data"}
        url={"https://antilopen.nl"}
        image={"/portfolio/antilopen.jpg"}
        id="antilopen"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek de website"
    >
        <p>
            <ul>
                <li>Externe data via de Sportclub Vrijwilligers Management API</li>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Snelheidsoptimalisatie met o.a. AWS CloudFront</li>
            </ul>
        </p>
    </PortfolioItem>
);
