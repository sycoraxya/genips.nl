import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioNewcold: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"NewCold"}
        subtitle={"Geoptimaliseerd voor iedere regio"}
        url={"https://jobs.newcold.nl"}
        image={"/portfolio/newcold.jpg"}
        id="newcold"
        imagePosition={imagePositionOverride || "left"}
        buttonText="Ontdek de website"
    >
        <p>
            <ul>
                <li>Dynamische data via JetEngine</li>
                <li>Social Media advertenties & marketing</li>
                <li>Meertaligheid & verschillende regio's</li>
                <li>Geavanceerde zoekmachine optimalisatie</li>
            </ul>
        </p>
    </PortfolioItem>
);
