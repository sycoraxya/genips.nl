import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioBijzonderecamping: FunctionComponent<
    React.PropsWithChildren<PortfolioProps>
> = ({ imagePositionOverride }) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"Bijzondere Camping"}
        subtitle={"De gamechanger voor de fysiotherapie"}
        url={""}
        image={"/portfolio/bijzonderecamping.jpg"}
        id="bijzonderecamping"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Binnenkort beschikbaar"
    >
        <p>
            <ul>
                <li>Dynamische data en kaarten via JetEngine</li>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Uniek ontwerp & illustraties</li>
                <li>Infrastructuur met AWS Lightsail & CloudFront</li>
                <li>Geavanceerde filters & navigatie</li>
            </ul>
        </p>
    </PortfolioItem>
);
