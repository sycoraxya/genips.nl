import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioAddVue: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"AddVue"}
        subtitle={"De nieuwe generatie contractmanagement software"}
        url={"https://addvue.com"}
        image={"/portfolio/addvue.jpg"}
        id="addvue"
        imagePosition={imagePositionOverride || "left"}
        buttonText="Ontdek de website"
    >
        <p>
            <ul>
                <li>Koppeling met AddVue bestelplatform</li>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Dynamische data via JetEngine</li>
                <li>Geavanceerde animaties</li>
            </ul>
        </p>
    </PortfolioItem>
);
