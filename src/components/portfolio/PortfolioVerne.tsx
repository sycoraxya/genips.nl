import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioVerne: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"Verne Health"}
        subtitle={"De gamechanger voor de fysiotherapie"}
        url={"https://vernehealth.nl"}
        image={"/portfolio/verne.jpg"}
        id="verne"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek de website"
    >
        <p>
            <ul>
                <li>Dynamische data via JetEngine</li>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Uniek ontwerp & illustraties</li>
            </ul>
        </p>
    </PortfolioItem>
);
