import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioSannewevers: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor]}
        title={"Sanne Wevers"}
        subtitle={"Topsport gegoten in een website"}
        url={"https://sannewevers.com"}
        image={"/portfolio/sannewevers.jpg"}
        id="sannewevers"
        imagePosition={imagePositionOverride || "right"}
        buttonText="Ontdek de website"
    >
        <p>
            <ul>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Uniek ontwerp voor een stralende uitstraling</li>
            </ul>
        </p>
    </PortfolioItem>
);
