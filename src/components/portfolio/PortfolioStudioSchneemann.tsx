import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioStudioSchneemann: FunctionComponent<
    React.PropsWithChildren<PortfolioProps>
> = ({ imagePositionOverride }) => (
    <PortfolioItem
        category={"website"}
        tags={[Tag.WordPress, Tag.Elementor, Tag.React]}
        title={"Studio Schneemann"}
        subtitle={"Een kunstige website voor een kunststudio"}
        url={"https://studioschneemann.com"}
        image={"/portfolio/schneemann.jpg"}
        id="schneemann"
        imagePosition={imagePositionOverride || "left"}
    >
        <p>
            <ul>
                <li>Custom ontwikkelde elementen met React</li>
                <li>Gemakkelijk in eigen beheer</li>
                <li>Eigenzinnige User Experience en animaties</li>
            </ul>
        </p>
    </PortfolioItem>
);
