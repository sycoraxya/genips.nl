import React, { FunctionComponent } from "react";
import { PortfolioItem, Tag } from "../portfolioItem";

interface PortfolioProps {
    imagePositionOverride?: "left" | "right";
}

export const PortfolioSVM: FunctionComponent<React.PropsWithChildren<PortfolioProps>> = ({
    imagePositionOverride,
}) => (
    <PortfolioItem
        category={"webapplicatie"}
        tags={[Tag.Nextjs, Tag.React, Tag.Nestjs, Tag.TypeScript, Tag.Nodejs]}
        title={"Sportclub Vrijwilligers Management"}
        subtitle={"Simpeler beheer en overzicht van jouw sportvereniging"}
        url={"https://sportclubvrijwilligersmanagement.nl"}
        image={"/portfolio/svm.jpg"}
        id="svm"
        imagePosition={imagePositionOverride || "left"}
        buttonText="Lees meer informatie"
    >
        <p>
            <ul>
                <li>API koppeling met verschillende sportbonden & Sportlink</li>
                <li>Ex- en importeren van data van en naar andere systemen</li>
                <li>Public Open API ontwikkeling</li>
                <li>Pushmeldingen naar je telefoon</li>
                <li>Geavanceerde permissiebeheer en databescherming</li>
            </ul>
        </p>
    </PortfolioItem>
);
