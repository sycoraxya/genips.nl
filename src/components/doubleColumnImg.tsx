import React, { FunctionComponent, ReactNode } from "react";
import styles from "../styles/5-components/doubleColumnImg.module.scss";

interface DoubleColumnImgProps {
    image: string;
    imageTitle: string;
    imageFit?: string;
    children: ReactNode;
    imagePosition?: "left" | "right";
}

export const DoubleColumnImg: FunctionComponent<React.PropsWithChildren<DoubleColumnImgProps>> = ({
    image,
    imageTitle,
    imageFit = "cover",
    children,
    imagePosition = "left",
}) => {
    return (
        <section className={styles.root} data-image-position={imagePosition}>
            <div className={styles.imageWrapper}>
                <img
                    loading="lazy"
                    src={image}
                    alt={imageTitle}
                    title={imageTitle}
                    data-image-fit={imageFit}
                />
            </div>
            <div className={styles.content}>{children}</div>
        </section>
    );
};
