import React, { FunctionComponent, useState } from "react";
import { useForm } from "react-hook-form";
import { MailResponse } from "../pages/api/mail";
import { emailRegex } from "../utils/emailValidate";
import styles from "../styles/5-components/contactForm.module.scss";
import { ChevronRight } from "./chevronRight";
import { Submit } from "./form/submit";
import { Check } from "./checkmark";
import { AnimatePresence, motion } from "framer-motion";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";

export enum Subject {
    Websites = "websites",
    Webshops = "webshops",
    Branding = "branding",
    Hosting = "hosting",
    Mailhosting = "mailhosting",
    Onderhoud = "onderhoud",
    Anders = "iets anders",
}

export interface ContactBody {
    fullName: string;
    companyName: string;
    subject: Subject;
    email: string;
    message: string;
    recaptchaResponseToken: string;
}

export const ContactForm: FunctionComponent<React.PropsWithChildren<unknown>> = () => {
    const { register, handleSubmit, formState, reset } = useForm<ContactBody>();
    const { errors } = formState;
    const [success, setSuccess] = useState(false);
    const [apiError, setApiError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const { executeRecaptcha } = useGoogleReCaptcha();
    const onSubmit = handleSubmit(async (data) => {
        setIsLoading(true);
        const recaptchaResponseToken = await executeRecaptcha("submit_form");

        data.recaptchaResponseToken = recaptchaResponseToken;

        const response = await fetch("/api/mail", {
            body: JSON.stringify(data),
            method: "post",
        });
        const json = (await response.json()) as MailResponse;

        if (!Object.keys(errors).length && json.success) {
            setSuccess(true);
            setApiError(false);
        } else {
            setSuccess(false);
            setApiError(true);
        }

        setIsLoading(false);
    });

    return (
        <section className={styles.root}>
            <form onSubmit={onSubmit} noValidate className={styles.form}>
                <div className={styles.formRow}>
                    <label htmlFor="fullName">Mijn naam is</label>
                    <input
                        placeholder="volledige naam"
                        type="text"
                        name="fullName"
                        id="fullName"
                        required
                        className={styles.input}
                        {...(errors.fullName ? { "data-error": true } : {})}
                        {...register("fullName", { required: true })}
                    />
                </div>
                <div className={styles.formRow}>
                    <label htmlFor="companyName" className={styles.hideLaptop}>
                        en ik werk bij
                    </label>
                    <label htmlFor="companyName" className={styles.showLaptop}>
                        Ik werk bij
                    </label>
                    <input
                        placeholder="bedrijfsnaam"
                        type="text"
                        name="companyName"
                        id="companyName"
                        required
                        className={styles.input}
                        {...(errors.companyName ? { "data-error": true } : {})}
                        {...register("companyName", { required: true })}
                    />
                </div>
                <div className={styles.formRow}>
                    <label htmlFor="subject" className={styles.hideLaptop}>
                        Voor mijn bedrijf ben ik op zoek naar
                    </label>
                    <label htmlFor="subject" className={styles.showLaptop}>
                        Ik ben op zoek naar
                    </label>
                    <span className={styles.selectWrap}>
                        <select
                            name="subject"
                            id="subject"
                            placeholder="selecteer"
                            required
                            className={styles.select}
                            {...(errors.subject ? { "data-error": true } : {})}
                            {...register("subject")}
                        >
                            {Object.keys(Subject)
                                .filter((key) => isNaN(+Subject[key]))
                                .map((key) => (
                                    <option value={key} key={key}>
                                        informatie over {Subject[key]}
                                    </option>
                                ))}
                        </select>
                        <ChevronRight color="rgba(0, 0, 0, 0.3)" size={26} />
                    </span>
                </div>
                <div className={styles.formRow}>
                    <label htmlFor="email">Je kunt mij bereiken via</label>
                    <input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="mailadres"
                        required
                        className={styles.input}
                        {...(errors.email ? { "data-error": true } : {})}
                        {...register("email", {
                            required: {
                                value: true,
                                message: "Email address is required",
                            },
                            pattern: {
                                value: emailRegex,
                                message: "Invalid email address",
                            },
                        })}
                    />
                </div>
                <div className={`${styles.formRow} ${styles.textAreaRow}`}>
                    <label htmlFor="message" className={styles.hideLaptop}>
                        Optioneel, wil ik nog even dit kwijt
                    </label>
                    <label htmlFor="message" className={styles.showLaptop}>
                        Overige informatie
                    </label>
                    <textarea
                        name="message"
                        id="message"
                        cols={30}
                        rows={6}
                        placeholder="bericht"
                        className={styles.textarea}
                        {...register("message")}
                    ></textarea>
                </div>
                <div className={styles.submitRow}>
                    <AnimatePresence>
                        {Object.keys(errors).length > 0 && !success && (
                            <motion.span
                                className={styles.error}
                                initial={{ opacity: 0 }}
                                animate={{ opacity: 1 }}
                                exit={{ opacity: 0 }}
                                transition={{ duration: 0.2 }}
                            >
                                Oeps, je bent enkele velden vergeten in te vullen!
                            </motion.span>
                        )}
                        {apiError && (
                            <motion.span
                                className={styles.error}
                                initial={{ opacity: 0 }}
                                animate={{ opacity: 1 }}
                                exit={{ opacity: 0 }}
                            >
                                Er is iets mis gegaan met het verzenden van je bericht. Probeer het
                                later nog eens of stuur een mail naar{" "}
                                <a href="mailto:hallo@genips.nl">hallo@genips.nl</a>
                            </motion.span>
                        )}
                    </AnimatePresence>
                    {success && (
                        <motion.span
                            className={styles.success}
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            transition={{ duration: 0.2 }}
                        >
                            Je bericht is verzonden!
                        </motion.span>
                    )}
                    <Submit success={success} isLoading={isLoading}>
                        {success ? (
                            <>
                                Gelukt! <Check />
                            </>
                        ) : (
                            <>
                                Verstuur <ChevronRight />
                            </>
                        )}
                    </Submit>
                </div>
            </form>
        </section>
    );
};
