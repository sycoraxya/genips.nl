import { FunctionComponent, PropsWithChildren } from "react";
import classnames from "classnames";
import styles from "../styles/5-components/double-column.module.scss";

interface DoubleColumnProps {
    className?: string;
}

export const DoubleColumn: FunctionComponent<React.PropsWithChildren<PropsWithChildren<DoubleColumnProps>>> = ({
    className,
    children,
}) => {
    return <section className={classnames(styles.root, className)}>{children}</section>;
};
