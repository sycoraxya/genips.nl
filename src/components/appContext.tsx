import { createContext, FunctionComponent, PropsWithChildren, useState } from "react";

export interface AppContextProps {
    heroColor: "black" | "white" | "green" | "light" | "dark";
    setHeroColor: (color: AppContextProps["heroColor"]) => void;
    isClientRouting: boolean;
    setIsClientRouting: (value: boolean) => void;
}

export const AppContext = createContext<AppContextProps>({
    heroColor: "black",
    setHeroColor: () => {},
    isClientRouting: false,
    setIsClientRouting: () => {},
});

export const AppContextProvider: FunctionComponent<React.PropsWithChildren<PropsWithChildren>> = ({ children }) => {
    const [heroColor, setHeroColor] = useState<AppContextProps["heroColor"]>("black");
    const [isClientRouting, setIsClientRouting] = useState<boolean>(false);

    return (
        <AppContext.Provider
            value={{
                heroColor,
                setHeroColor: (color: AppContextProps["heroColor"]) => {
                    setHeroColor(color);
                },
                isClientRouting,
                setIsClientRouting: (value: boolean) => {
                    setIsClientRouting(value);
                },
            }}
        >
            {children}
        </AppContext.Provider>
    );
};
