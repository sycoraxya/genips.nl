import { FunctionComponent } from "react";
import Link from "next/link";
import styles from "../styles/5-components/animatedLink.module.scss";
import classnames from "classnames";

interface AnimatedLinkProps {
    href: string;
    title: string;
    type?: "dark" | "light";
    external?: boolean;
    className?: string;
}

export const AnimatedLink: FunctionComponent<React.PropsWithChildren<AnimatedLinkProps>> = ({
    href,
    title,
    type,
    external,
    className,
}) => {
    const cx = classnames(styles.root, { [className]: !!className });

    if (!external) {
        return (
            (<Link
                href={href}
                scroll={false}
                data-hover={title}
                className={cx}
                data-type={type || "dark"}>

                <span>{title}</span>

            </Link>)
        );
    }

    return (
        <a
            data-hover={title}
            href={href}
            className={cx}
            data-type={type || "dark"}
            target="_blank"
            rel="noopener noreferrer"
        >
            <span>{title}</span>
        </a>
    );
};
