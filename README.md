# Artikel maken

Maak in `src/pages/insights` een nieuw bestand aan, bijvoorbeeld: `wat-is-webdesign.mdx` en voeg daar de volgende boilerplate aan toe:

```tsx
import { ArticleLayout, ArticleTags, Picture } from "@components/article";
import styles from "@styles/insights/article.module.scss";

export const meta = {
    title: "Wat is webdesign",
    description: "En hoe kan het jou helpen",
    date: new Date(2021, 0, 24),
    tags: [ArticleTags.design, ArticleTags.development, ArticleTags.website],
    heroImage: { path: "/insights/wat-is-webdesign/", filename: "klok" },
    slug: "wat-is-webdesign",
};

// Hier komt de content. Gebruik hiervoor <Section></Section> en verder normale markdown. Voor afbeeldingen gebruik je de <Picture /> tag

export default ({ children }) => (
    <ArticleLayout
        title={meta.title}
        description={meta.description}
        date={meta.date}
        heroImage={meta.heroImage}
        tags={meta.tags}
        slug={meta.slug}
    >
        {children}
    </ArticleLayout>
);
```

Voor de opbouw van de pagina gebruik je sections:

```md
<Section>

## Dit is een koptekst

Hier hebben we een paragraaf. De content is in normale markdown.

Zorg er wel altijd voor dat er witruimte om de markdown heen is!

</Section>
```

En voor afbeeldingen de `<Picture />` tag:

```tsx
<Section>
    <Picture
        sources={[
            {
                srcSet: "/insights/wat-is-webdesign/afbeelding_small.svg",
                media: "(max-width: 468px)",
            },
        ]}
        img={{
            src: "/insights/wat-is-webdesign/afbeelding_small.svg",
            alt: "Afbeelding",
            width: 834,
            height: 530,
        }}
    />
</Section>
```

# Building & pushing

docker-compose -f docker-compose.prod.yml build

Push to registry

# Pulling & deploying

SSH to genips

```bash
docker pull registry.gitlab.com/sycoraxya/genips.nl:latest
```

Restart image in plesk

## Staging

docker-compose -f docker-compose.staging.yml build

docker push registry.gitlab.com/sycoraxya/genips.nl:staging

## Staging on AWS

docker-compose -f docker-compose.staging-aws.yml build

docker push 137750540333.dkr.ecr.eu-west-1.amazonaws.com/genips:staging

### Pulling & deploying

SSH to genips

```bash
docker pull registry.gitlab.com/sycoraxya/genips.nl:staging
```

Restart image in plesk
